### **TODO**
<hr>

# **Logique**

## Global
    [ ] Detecter et debugger deconnexion intempestive  
    [ ] Detecter la présence ou non des images d'instruments (intrument_image_url), et ajouter deux champs  dans la table instrument
        [ ] {String} - image_url - Lien vers l'image de l'instrument
        [ ] {Boolean} - isPresent - Si le lien vers l'image fonctionne ou non
    [x] Remplacer les genres par des avatars (d'animaux ? :)
        [ ] Créer une table avatar avec (name, path, gender)
        [ ] Créer une sélection des avatars coté front
    [ ] Vérifier la création auto des styles et des instruments coté back
    [x] Créer un objet title (a afficher dans le header)
    [ ] Créer des roles pour l'acces 'admin' et 'modo'
        - Admin => add news, add category, style controller (ADD-DELETE-LIST), instrument-controller (ADD-DELETE-LIST)
        - Writer => add news, add category
    [x] Récuperer et mettre a jour les données de géolocalisation
    [ ] Utiliser les flashMessages
    [ ] Ajouter email au form d'edition
    [ ] Debugger le retour du form edit vers l'account non home page
    [ ] Ajouter la partie administrateur

## Auth
    [ ] Simplifier la création du compte (name, email, password, gender/avatar)
    [ ] Ajouter l'acceptation de recevoir des infos (si email renseigné)
    [ ] Ajouter personnalisation du compte, après l'inscription
        [ ] Ajouter les envies d'apprendre
        [ ] Ajouter le style le plus ecouté
        [ ] Ajouter le style le plus pratiqué
        [ ] Ajouter les instrumets pratiqués
    [ ] Ajouter la validation du compte ? (si email renseigné)
    [ ]

## Messagerie
    [x] Ajouter la possibilité d'envoyer des messages
        [x] Créer un objet message
        [x] Créer un objet conversation
    [x] Debugger redirection apres un nouveau message  
    [ ]

## Map
    [x] Gérer les redirections GET vers la map (lat lng) si on vient de la user-list
    [x] Faire en sorte que si la lat et la lng ne sont pas renseignée, on prend celle de l'user connecté
    [ ] Ajouter des filtres sur la map
        [ ] Ajouter des filtres sur la map
    [ ]

## Account
    [ ] Gérer les redirections vers la fiche d'utilisateur (composant similaire a la user-card dans la vue user-list)
    [ ] 

<br>
<hr>

# **Design**

## Global
    [x] Afficher l'objet title dans le header
    [ ] Redesigner globalement l'application
    [ ] Placer les images dans un dossier app (debugger les 404 au niveau des images)
    [ ]


## Image
    [x] Récuperer des images de profil (avatar)   
    [x] Récuperer des images d'instruments (intrument_image_url)  