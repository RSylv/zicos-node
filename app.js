// PWA CONFIG
const fs = require('fs')
const https = require('https')

const httpPort = 8080
const httpsPort = 4430
const key = fs.readFileSync('./certs/localhost.key')
const cert = fs.readFileSync('./certs/localhost.crt')

const app = require('./app-config')
const config = require('./api/config/server')

// PWA CONFIG
const server = https.createServer({ key, cert }, app)
app.use((req, res, next) => {
  if (!req.secure) {
    return res.redirect('https://' + req.headers.host + req.url)
  }
  next()
})

if (process.env.NODE_ENV !== 'production') {
  const Loaderlog = require('./api/module/LoaderConsole')
  const loader = new Loaderlog()
  loader.color = 'magenta'
  loader.log('*')
}

// LISTENNING
app.listen(config.port, () => {
  console.log('\x1b[34m******************************\x1b[0m')
  console.log('\x1b[44m°                            °\x1b[0m')
  console.log('\x1b[44m ☠  Application is running... \x1b[0m')
  console.log('\x1b[44m.                            .\x1b[0m')
  console.log('\x1b[34m******************************\x1b[0m')
  console.log(`\x1b[34m ☠  ${config.base_url}  ☠  \x1b[0m`)
  console.log('\x1b[34m******************************\x1b[0m')
  console.log('\x1b[34m ☠  Connecting to mongoose...\x1b[0m')
  console.log('\x1b[34m______________________________\x1b[0m')
})

// PWA CONFIG
app.listen(httpPort, function () {
  console.log(`Listening on port http://localhost:${httpPort}`)
})
server.listen(httpsPort, function () {
  console.log(`Listening on port https://localhost:${httpsPort}`)
})
