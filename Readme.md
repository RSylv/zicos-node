# Music pro and guest community :
# 🍄  Zicos.com  ♪♪  
------------------------------------------
## [EXPRESS DOC](http://expressjs.com/en/4x/api.html#res.redirect)
------------------------------------------
## [REPO GITHUB - (link)](https://github.com/6-L-20/zicos.git ) 
------------------------------------------
### ⚠️ **Start Service mongodb.exe on windows if ERR_CONN**
------------------------------------------

<br>
<br>

## Run project :

> Install dependancies
```
npm i
```

> INSTALL MONGO DB - (**mac-OsX**) - [docLink](https://www.mongodb.com/docs/v4.4/tutorial/install-mongodb-on-os-x/)
```
brew tap mongodb/brew
```
```
brew update  
```
```
arch -arm64 brew install mongodb-community@4.4
```

> Lancer MongoDb - (**mac-OsX**)
```
brew services start mongodb-community@4.4
```

> Lancer Le projet (*local*)
```
npm run devStart
```


# Architecture du projet:
### **NodeJs(Express) - Ejs - MongoDB**
## MVC 

- model/
    - article.js
    ( Modele article construit selon le schema mongoDB, Donc lié a la table. )
    - ...
- routes/
    - article.js
    ( Fichier de functions, qui gere les routes. "/", "article/12", "article/new", et leurs logique. C'est la partie Controller. )
    - ...
- views/
    -article/
        index.ejs 
        add.ejs 
        ( Fichier ecrit en html au format ejs (permet l'insertion de variables.) C'est la Vue. )
        - ...


### Models (DB):
- Users
    - pseudo
    - place
    - password
    - level
    - sexe
    - connected
    - created_at
    - instrument
    > @ORM\ManyToOne(targetEntity=Styles)
    - styles
    > @ORM\OneToMany(targetEntity=news)
    - news 

- News
    - title
    - content
    - created_at
    - image_url
    - modified_at    
    > @ORM\ManyToOne(targetEntity=Category)
    - category 
    > @ORM\OneToMany(targetEntity=Comment)
    - comments
    > @ORM\ManyToOne(targetEntity=Users)
    - user_id 

- Category
    - name
    - description
    > @ORM\OneToMany(targetEntity=news)
    - news

- Comment
    - author
    - content
    - created_at
    > @ORM\ManyToOne(targetEntity=news)
    - news

- Style
    - name
    > @ORM\OneToMany(targetEntity=Users)
    - users
-------------------------------------------------------------------


### Routes:
routes:
- news
- category
- zicos
- styles
- index
             



## COLORS:

--white: #fff;
--primary: #3a3f44;
--secondary: #7a8288;
--light: #e9ecef;
--dark: #272b30;

--success: #62c462;
--info: #5bc0de;
--warning: #f89406;
--danger: #ee5f5b;



#### MODULES INSTALLED
-> npm install express
-> npm install ejs
-> npm install express-ejs-layouts
-> npm install mongoose
-> npm install method-override
-> npm install body-parser
-> npm install slugify
<!-- -> npm install passport -->
<!-- -> npm install passport-local -->
<!-- -> npm install passport-local-mongoose -->
<!-- -> npm install express-sessions -->
--------------------------------------
-> npm install bcryptjs
-> npm install jsonwebtoken
-> npm install cookie-parser



<!-- AUTH METHOD: -->
<!-- body-parser (for parsing incoming requests) -->
<!-- express (to make the application run) -->
<!-- mongoose (object data modeling to simplify interactions with MongoDB) -->
<!-- express session (to handle sessions) -->
<!-- passport-local -->
<!-- passport-local-mongoose -->



<option value="9">DIY</option>
<option value="10">Studio Engineering</option>
<option value="11">Learning Instrument Tips</option>
<option value="13">News</option>
<option value="14">HighTech</option>




### Code couleurs console log
```
const COLOR = {
  reset: '\x1b[0m',
  bright: '\x1b[1m',
  dim: '\x1b[2m',
  underscore: '\x1b[4m',
  blink: '\x1b[5m',
  reverse: '\x1b[7m',
  hidden: '\x1b[8m',

  fgBlack: '\x1b[30m',
  fgRed: '\x1b[31m',
  fgGreen: '\x1b[32m',
  fgYellow: '\x1b[33m',
  fgBlue: '\x1b[34m',
  fgMagenta: '\x1b[35m',
  fgCyan: '\x1b[36m',
  fgWhite: '\x1b[37m',

  bgBlack: '\x1b[40m',
  bgRed: '\x1b[41m',
  bgGreen: '\x1b[42m',
  bgYellow: '\x1b[43m',
  bgBlue: '\x1b[44m',
  bgMagenta: '\x1b[45m',
  bgCyan: '\x1b[46m',
  bgWhite: '\x1b[47m',
};

console.log(`${COLOR.fgRed}This text is red.${COLOR.reset}`);
```



## Reperage sur map et instruments/styles :
- [Instrument] [Style]
- Ex. Chant, Metal  

<br>
<br>
<br>

## ENV file :  
<br>  

> DATABASE_URL = mongodb://localhost/zicos    
> SECRET_KEY = 'SecretJwtKey'    
> SESSION_SECRET = 'SecretSessionKey'   
> PORT = 1337


<br>
<br>
<br>
<br>

## PWA CONFIG :     

[PWA EXPRESS TUTO](https://blog.logrocket.com/how-to-build-a-progressive-web-app-pwa-with-node-js/) 



## PUSH

git add .
git commit -m 'blabla'
git push -u origin main
git push heroku main:master