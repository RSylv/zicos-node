const fs = require('fs')
const path = require('path')
const Instrument = require('../models/instrument')
const InstrumentCategory = require('../models/instrumentCategory')
const utils = require('../module/utils')

const maxLevel = 2
const mappedResult = {}
const mappedRules = [
  // PERCUSSION
  { ref: 'percussion', level: 1, family: 'percussion' },
  { ref: 'batterie', level: 1, family: 'percussion' },
  { ref: 'tambour', level: 1, family: 'percussion' },
  { ref: 'cymbale', level: 1, family: 'percussion' },
  { ref: 'conga', level: 1, family: 'percussion' },
  { ref: 'bongo', level: 1, family: 'percussion' },
  { ref: 'darbouka', level: 1, family: 'percussion' },
  { ref: 'hochet', level: 2, family: 'percussion' },
  { ref: 'caisse claire', level: 2, family: 'percussion' },
  { ref: 'claves', level: 2, family: 'percussion' },
  { ref: 'cloche', level: 2, family: 'percussion' },
  { ref: 'cymbalum', level: 2, family: 'percussion' },
  { ref: 'manjira', level: 2, family: 'percussion' },
  { ref: 'manjira', level: 2, family: 'percussion' },
  { ref: 'marimba', level: 2, family: 'percussion' },
  { ref: 'drum', level: 2, family: 'percussion' },
  { ref: 'planche', level: 2, family: 'percussion' },
  { ref: 'tam-tam', level: 2, family: 'percussion' },
  { ref: 'timba', level: 2, family: 'percussion' },
  { ref: 'repinique', level: 2, family: 'percussion' },
  // CORDES
  { ref: 'guitar', level: 1, family: 'corde' },
  { ref: 'lyre', level: 1, family: 'corde' },
  { ref: 'cithare', level: 1, family: 'corde' },
  { ref: 'mandoline', level: 1, family: 'corde' },
  { ref: 'banjo', level: 1, family: 'corde' },
  { ref: 'cavaquinho', level: 1, family: 'corde' },
  { ref: 'harpe', level: 1, family: 'corde' },
  { ref: 'ukulele', level: 1, family: 'corde' },
  { ref: 'basse', level: 2, family: 'corde' },
  { ref: 'contrebass', level: 2, family: 'corde' },
  { ref: 'mando', level: 2, family: 'corde' },
  { ref: 'pipa', level: 2, family: 'corde' },
  { ref: 'luth', level: 2, family: 'corde' },
  { ref: 'viele', level: 2, family: 'corde' },
  { ref: 'viola', level: 2, family: 'corde' },
  { ref: 'lira', level: 2, family: 'corde' },
  { ref: 'requinto', level: 2, family: 'corde' },
  { ref: 'violon', level: 2, family: 'corde' },
  // VENT
  { ref: 'vent', level: 1, family: 'vent' },
  { ref: 'accordeon', level: 1, family: 'vent' },
  { ref: 'flute', level: 1, family: 'vent' },
  { ref: 'hautbois', level: 1, family: 'vent' },
  { ref: 'souffle', level: 1, family: 'vent' },
  { ref: 'cornemuse', level: 1, family: 'vent' },
  { ref: 'harmonica', level: 1, family: 'vent' },
  { ref: 'guimbarde', level: 1, family: 'vent' },
  { ref: 'clarinette', level: 1, family: 'vent' },
  { ref: 'saxophone', level: 1, family: 'vent' },
  { ref: 'soubassophone', level: 1, family: 'vent' },
  { ref: 'trombone', level: 1, family: 'vent' },
  { ref: 'trompette', level: 1, family: 'vent' },
  { ref: 'tuba', level: 1, family: 'vent' },
  { ref: 'clairon', level: 2, family: 'vent' },
  { ref: 'conque', level: 2, family: 'vent' },
  { ref: 'corne', level: 2, family: 'vent' },
  { ref: 'cor', level: 2, family: 'vent' },
  { ref: 'pipeau', level: 2, family: 'vent' },
  { ref: 'piccolo', level: 2, family: 'vent' },
  { ref: 'musette', level: 2, family: 'vent' },
  { ref: 'saxhorn', level: 2, family: 'vent' },
  // PIANO
  { ref: 'piano', level: 1, family: 'piano' },
  { ref: 'clavecin', level: 1, family: 'piano' },
  { ref: 'clavinet', level: 1, family: 'piano' },
  { ref: 'harmonium', level: 1, family: 'piano' },
  { ref: 'clavier', level: 1, family: 'piano' },
  { ref: 'orgue', level: 1, family: 'piano' },
  { ref: 'manichordion', level: 2, family: 'piano' },
  { ref: 'percuphone', level: 2, family: 'piano' },
  // ELECTRONIQUE
  { ref: 'synthetiseur', level: 1, family: 'electronique' },
  { ref: 'electronique', level: 2, family: 'electronique' },
  { ref: 'stylophone', level: 2, family: 'electronique' },
  // CHANT
  { ref: 'chant', level: 2, family: 'chant' },
  { ref: 'choeur', level: 2, family: 'chant' },
  { ref: 'choriste', level: 2, family: 'chant' },
  // AUTRE
  { ref: 'bol chantant', level: 1, family: 'autre' }
]

function getOnlyName (str) {
  const result = str.split(':')[0].trim()
  result.split('(')[0].trim()
  return result.split('\n').map(r => r.trim()).join(' ')
}

function getLength (array, type) {
  return type === 'mapped'
    ? array.map(e => e.mapped && e).filter(n => n).length
    : type === 'unmapped'
      ? array.map(e => !e.mapped && e).filter(n => n).length
      : type === 'total'
        ? array.length
        : 'error: unrecognize type'
}

function mappedResultLength (mappedResult) {
  let total = 0
  Object.keys(mappedResult).forEach(k => {
    total += mappedResult[k].data.length
  })
  return total
}

function mapInstruments (instrumentsName, level = 1) {
  console.log(`IN ${level}: `, {
    mapped: getLength(instrumentsName, 'mapped'),
    unmapped: getLength(instrumentsName, 'unmapped')
  })
  if (level > maxLevel) {
    console.log('OUT: ', {
      total: getLength(instrumentsName, 'total'),
      mappedResultLength: mappedResultLength(mappedResult)
    })
    return mappedResult
  }
  for (let i = 0; i < instrumentsName.length; i++) {
    if (!instrumentsName[i].mapped) {
      const formattedName = instrumentsName[i].name.toLowerCase().trim().normalize('NFD').replace(/[\u0300-\u036f]/g, '')

      const isIn = mappedRules.find(element => element.level === level && formattedName.includes(element.ref))

      if (isIn) {
        mappedResult[isIn.family].data.push(getOnlyName(instrumentsName[i].name))
        instrumentsName[i].mapped = true
      } else if (level === maxLevel) {
        mappedResult.autre.data.push(getOnlyName(instrumentsName[i].name))
      }
    }
  }
  mapInstruments(instrumentsName, ++level)
}

module.exports = {

  writeMappedInstrumentFile: (filename, content) => {
    fs.writeFile(
      filename,
      content,
      function (err) {
        if (err) throw new Error(`writeMappedInstrumentFile: ${err}`)
        console.log('Fichier créé !')
      }
    )
  },

  saveInstrumentCategoryOnDB: async () => {
    const categories = [
      { name: 'percussion', imageUrl: 'percussion.svg' },
      { name: 'corde', imageUrl: 'cord.svg' },
      { name: 'vent', imageUrl: 'wind.svg' },
      { name: 'piano', imageUrl: 'piano.svg' },
      { name: 'electronique', imageUrl: 'electronic.svg' },
      { name: 'chant', imageUrl: 'vocal.svg' },
      { name: 'autre', imageUrl: 'other.svg' }
    ]

    // Compare categories instruments before saved
    const getCategories = await InstrumentCategory.find({})
    if (getCategories && getCategories.length) {
      for (let i = 0; i < categories.length; i++) {
        for (let y = 0; y < getCategories.length; y++) {
          if (categories[i].name === getCategories[y].name) {
            categories.splice(i, 1)
          }
        }
      }
    }

    // Save the categories if necessary
    if (categories.length) {
      return InstrumentCategory.insertMany(
        categories.map((obj) => {
          return {
            name: obj.name,
            slugName: utils.slugify(obj.name),
            imageUrl: `/image/instrument/${obj.imageUrl}`
          }
        }, (_err, data) => {
          if (_err) throw new Error(`saveInstrumentCategoryOnDB: ${_err}`)
          console.log('InstrumentCategories créé !', data)
        })
      )
    } else {
      return { message: 'Nothing to update!' }
    }
  },

  saveInstrumentOnDB: async (instrumentName = null, onlymapping = false) => {
    let uniqueInstrumentData = null
    if (!instrumentName) {
      // Get all instruments from file
      const instrumentData = JSON.parse(
        fs.readFileSync(
          path.join(__dirname, '/instrumentMapper/instruments.json'),
          'utf8'
        )
      )

      // Get unique instruments name collection
      uniqueInstrumentData = [...new Set(instrumentData)]
    } else {
      uniqueInstrumentData = [instrumentName]
    }

    // Get all categories from DB
    const categories = await InstrumentCategory.find({})
    categories.forEach(
      cat => {
        mappedResult[cat.name] = {
          id: cat.id,
          data: []
        }
      }
    )

    // Map all instruments with categories
    mapInstruments(
      uniqueInstrumentData.map(
        instrument => {
          return {
            name: instrument,
            mapped: false
          }
        }
      )
    )

    // Build mongoose instrument object
    const instrumentsAndCategoryToSave = []
    Object.keys(mappedResult).forEach(key => {
      mappedResult[key].data.forEach(name => {
        instrumentsAndCategoryToSave.push({
          name,
          slugName: utils.slugify(name),
          category: mappedResult[key].id,
          mapped: key !== 'autre'
        })
      })
    })

    // Return only mapping if boolean is true
    if (onlymapping) {
      return instrumentsAndCategoryToSave
    }

    // Get All Instruments fromd DB and compare before saved
    const alreadySavedInstruments = await Instrument.find({})
    if (alreadySavedInstruments && alreadySavedInstruments.length) {
      for (let i = 0; i < alreadySavedInstruments.length; i++) {
        for (let y = 0; y < instrumentsAndCategoryToSave.length; y++) {
          if (
            alreadySavedInstruments[i].slugName &&
            instrumentsAndCategoryToSave[y].slugName
          ) {
            if (instrumentsAndCategoryToSave[y].slugName === alreadySavedInstruments[i].slugName) {
              instrumentsAndCategoryToSave.splice(y, 1)
            }
          } else {
            console.log(instrumentsAndCategoryToSave[y], alreadySavedInstruments[i])
          }
        }
      }
    }

    // Save all instruments with the right category if necessary
    if (instrumentsAndCategoryToSave && instrumentsAndCategoryToSave.length) {
      try {
        const saved = await Instrument.insertMany(instrumentsAndCategoryToSave)
        return saved
      } catch (error) {
        throw new Error(`saveInstrumentOnDB: ${error}`)
      }
    } else {
      return { message: 'Nothing to update!' }
    }
  }

}
