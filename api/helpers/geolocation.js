/**
 * This module request the api ipwhois,
 */
const axios = require('axios').default
const { ipwhois, cityLatLng } = require('../config/server')
const _ = require('lodash')
const debug = require('debug')
const DEBUG = debug('dev')

function apiRequest (ip) {
  return new Promise((resolve, reject) => {
    axios
      .request({
        method: 'GET',
        params: { ip },
        url: ipwhois.url,
        headers: {
          'x-rapidapi-host': ipwhois.host,
          'x-rapidapi-key': ipwhois.key
        }
      })
      .then((res) => {
        console.log('\nAPI-RESPONSE : \n', res.data ? '\x1b[32m ☑  data recieved ⛱  \x1b[0m\n' : '\x1b[31m ⬛  no data... \x1b[0m\n')
        resolve(res.data)
      })
      .catch((e) => {
        console.error('ERR - apiRequest : ', e)
        reject(e)
      })
  })
}

module.exports = {
  getFullPositionByIp: async (ip) => {
    return await apiRequest(ip)
  },

  getFormattedPositionIp: async (ip) => {
    return _.pick(
      await apiRequest(ip),
      [
        'ip',
        'region',
        'city',
        'country',
        'country_capital',
        'latitude',
        'longitude'
      ]
    )
  },

  getLatLng: async (place) => {
    return new Promise((resolve, reject) => {
      axios.get(`${cityLatLng.url}${place}`)
        .then(res => {
          const data = res.data.length ? res.data[0] : res.data
          resolve(data)
        })
        .catch(err => {
          DEBUG(`Error getting the lat and lng of the user: ${err.message || err}`)
          resolve(false)
        })
    })
  }
}
