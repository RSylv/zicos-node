const utils = require('../module/utils')
const Conversation = require('../models/conversation')
const User = require('../models/user')

module.exports = {

  /**
     * @param {String} conversationId
     * @returns {Promise<Object>}s
     */
  getOrCreateConversationObject: async (conversationId = null, name = null) => {
    try {
      if (utils.isValidMongoId(conversationId)) {
        return { conversation: await Conversation.findById(conversationId), isNew: false }
      } else {
        return { conversation: new Conversation({ groupName: name }), isNew: true }
      }
    } catch (e) {
      console.log('Err - getOrCreateConversationObject : ', e)

      throw new Error('Err - getOrCreateConversationObject : ', e.message)
    }
  },

  /**
     * @param {Object} conversation
     * @param {Object} toUser s
     * @param {Object} fromUser
     * @returns {Object}
     */
  addUsersToConversation: (conversation, toUser, fromUser) => {
    conversation.users.indexOf(fromUser.id) === -1 && conversation.users.push(fromUser.id)
    conversation.users.indexOf(toUser.id) === -1 && conversation.users.push(toUser.id)
    conversation.lastWriter = fromUser.username

    return conversation
  },

  /**
     * @param {Object} conversation
     * @param {Object} message
     * @param {Boolean} isNew
     * @returns {Object}
     */
  addMessageToConversationAndSave: async (conversation, message, isNew = false) => {
    if (typeof isNew !== 'boolean') { throw new Error("'isNew' has to be a boolean value.") }

    try {
      conversation.messages.push(message)

      if (!isNew) {
        return await Conversation.findOneAndUpdate({ _id: conversation._id }, {
          messages: conversation.messages,
          users: conversation.users,
          lastWriter: conversation.lastWriter
        }, { new: true }).populate('messages').populate('users', 'username')
      } else {
        return await conversation.save()
      }
    } catch (e) {
      console.log('Err - addMessageToConversationAndSave : ', e)

      throw new Error('Err - addMessageToConversationAndSave : ', e.message)
    }
  },

  /**
     * @param {Object} user
     * @returns {Object[]}
     */
  getAllUserConversations: async (user, newConversation = null) => {
    try {
      const allConversations = []

      if (user.conversations && user.conversations.length) {
        await Promise.all(user.conversations.map(async (convId, i) => {
          const conversation = await Conversation.findById(convId)
          if (conversation && conversation.messages && conversation.messages.length) {
            await conversation.messages[0].populate('to')
            await conversation.messages[0].populate('from')
            await conversation.populate('users')
            allConversations.push(conversation)
          } else {
            console.log('\n⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️\nNO-MESSAGES::\n', convId)
          }
        }))
      }

      newConversation && allConversations.push(newConversation)

      return allConversations.sort((a, b) => b.createdAt - a.createdAt)
    } catch (e) {
      console.log('Err - getAllUserConversations : ', e)

      throw new Error('Err - getAllUserConversations : ', e.message)
    }
  },

  updateUserConversations: async (toUser, fromUser, conversation) => {
    try {
      fromUser.conversations.push(conversation)
      await User.updateOne({ _id: fromUser._id }, { conversations: fromUser.conversations }).exec()
      toUser.conversations.push(conversation)
      await User.updateOne({ _id: toUser._id }, { conversations: toUser.conversations }).exec()
    } catch (e) {
      console.log('Err - updateUserConversations : ', e)

      throw new Error('Err - updateUserConversations : ', e.message)
    }
  },

  deleteUserConversation: async (user, conversationId) => {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve, reject) => {
      try {
        const users = await User.find({ conversations: conversationId }).populate('conversations', 'id').exec()

        if (!users && !users.length) { return reject(new Error('deleteUserConversation - no user found..')) }

        const ok = await Promise.all(users.map(async u => {
          u.conversations = u.conversations.filter(c => c.id !== conversationId).map(u => u.id)
          await u.save()
        }))

        return resolve(ok)
      } catch (e) {
        console.log('Err - deleteUserConversation : ', e)

        throw new Error('Err - deleteUserConversation : ', e.message)
      }
    })
  }

}
