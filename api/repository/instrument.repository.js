/* eslint-disable camelcase */
/* eslint-disable no-async-promise-executor */
const Instrument = require('../models/instrument')
const mapAndSaveInstrument = require('../helpers/instrument-mapper').saveInstrumentOnDB
const validation = require('../helpers/validations')
const _ = require('lodash')
const utils = require('../module/utils')
const News = require('../models/news')

module.exports = {

  getInstruments: async (onlyName = false) => {
    return new Promise(async (resolve, reject) => {
      try {
        const instruments = await Instrument
          .find()
          .exec()
        resolve(onlyName ? instruments.map(i => i.name) : instruments)
      } catch (e) {
        console.error('ERR - allInstrument : ', e)
        reject(new Error('Error getting instruments\n' + (e.message || 'no message')))
      }
    })
  },

  /**
     * @param {String} name
     * @param {String} id
     * @returns {Object|String}
     */
  getInstrument: async (name = null, id = null) => {
    return new Promise(async (resolve, reject) => {
      let instrument = null
      try {
        if (!_.isEmpty(name) && typeof name === 'string') {
          instrument = await Instrument
            .findOne({
              $or: [
                { name: name.toString().toLowerCase() },
                { slugName: name }
              ]
            })
            .exec()
        } else if (id && typeof name === 'string' && validation.validateMongooseId(id)) {
          instrument = await Instrument
            .findById(id)
            .exec()
        } else {
          reject(new Error('You have to give a name or an id\ntype: String'))
        }
        resolve(instrument)
      } catch (e) {
        console.error('ERR - Get a news : ', e)
        reject(new Error('error processing the entity Instrument\n' + (e.message || 'no message')))
      }
    })
  },

  getInstrumentEmptyObject: () => {
    return new Instrument()
  },

  /**
     * @param {String} name
     * @returns
     */
  createInstrument: async (name) => {
    return new Promise(async (resolve, reject) => {
      try {
        if (!name || typeof name !== 'string') {
          reject(new Error('name must be a string'))
        }
        const instrument = await mapAndSaveInstrument(name, true)
        const instrumentSave = await new Instrument({
          name: instrument[0].name,
          slugName: instrument[0].slugName,
          category: instrument[0].category,
          mapped: instrument[0].mapped
        })
        resolve(instrumentSave)
      } catch (e) {
        console.log('ERR - createInstrument: ', e)
        reject(new Error('Error creating news' + (e.message || 'no message')))
      }
    })
  },

  /**
     * @param {String} id
     * @returns
     */
  deleteOne: async (id) => {
    return new Promise(async (resolve, reject) => {
      try {
        if (!id && typeof id !== 'string' && !validation.validateMongooseId(id)) { reject(new Error('Invalid or Unknown Id')) }
        await News.findByIdAndDelete(id)
        resolve(true)
      } catch (e) {
        console.log('ERR - deleteOne: ', e)
        reject(new Error('Error during deletion: ' + (e.message || 'no message')))
      }
    })
  },

  /**
     * @param {String} instruments
     * @returns {Promise<String|Boolean>}
     */
  saveInstrumentsAndReturn: async function (instruments) {
    instruments = instruments.split(',')
      .map(ins => ins.trim()
        .toLowerCase())
      .filter(n => n)

    instruments = [...new Set(instruments)]

    console.log('INSTRUMENTS RECIEVED:: \n', instruments)

    const newInstruments = await Promise.all(
      instruments.map(
        async (name) => {
          const slugName = utils.slugify(name)
          try {
            if (slugName && slugName !== '') {
              const alreadySave = await module.exports.getInstrument(slugName)
              console.log(`${alreadySave && alreadySave._id ? '✓' : '🗳'} Instrument ${name} ${alreadySave && alreadySave._id ? 'is already saved' : 'is going to be saved'}.`)
              if (!alreadySave) {
                const instr = await module.exports.createInstrument(name)
                return instr
              }
              return alreadySave
            }
            return false
          } catch (e) {
            console.error('ERR - Register - save instrument : ', e)
            return false
          }
        })
    )

    const result = await newInstruments.filter(n => n)

    return {
      instrIds: result.map(i => i.id),
      instrNames: result.map(i => i.name).join(', ').trim()
    }
  },

  getInstrumentImageUrl: (instrument_text) => {
    const text = instrument_text.split(',')[0].trim() || 'image_base'
    return `/image/instrument/${text}.png`
  }

}
