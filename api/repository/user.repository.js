/* eslint-disable camelcase */
const User = require('../models/user')

module.exports = {

  getAllUser: async () => {
    try {
      const users = await User.find()
      return users || []
    } catch (e) {
      console.log('ERR - getAllUser : ', e)
    }
  },

  getUser: async (id) => {
    try {
      const user = await User.findById(id)
      return user || false
    } catch (e) {
      console.log('ERR - getUser : ', e)
    }
  },

  getUserByUsername: async (username) => {
    try {
      const user = await User.findOne({ username })
      return user || false
    } catch (e) {
      console.log('ERR - getUserByUsername : ', e)
    }
  },

  getUserBySearchTerm: async (search_terms) => {
    try {
      const users = search_terms
        ? await User.find({
          $or: [
            { username: search_terms },
            { place: search_terms },
            { style_text: search_terms },
            { instruments_text: search_terms }
          ]
        }).limit(100)
        : await User.find().limit(100)

      return users
    } catch (e) {
      console.log('ERR - getUserBySearchTerm : ', e)
    }
  },

  updateOne: async (user) => {
    try {
      if (!user.id) {
        throw new Error('Can\'t update user without id provided.')
      }
      return await User.findOneAndUpdate(
        { _id: user.id },
        user,
        {
          upsert: true,
          returnDocument: 'after'
        }
      )
    } catch (e) {
      console.log('ERR - updateOne : ', e)
    }
  },

  createUser: async (data) => {
    try {
      const user = new User(data)
      return await user.save()
    } catch (e) {
      console.log('ERR - createUser : ', e)
    }
  },

  deleteUser: async (_id) => {
    try {
      const user = await User.findByIdAndDelete(_id)
      console.log('RES: delete-user : ', user)
      if (!user) {
        return false
      }
      return user
    } catch (e) {
      console.log('ERR - deleteUser : ', e)
    }
  },

  updateUserPosition: async (id, locationData) => {
    return new Promise(resolve => {
      User.findOneAndUpdate(
        { _id: id },
        { $set: { ...locationData } },
        { new: true }, (err, user) => {
          if (err) {
            console.log('ERR - updateUserLocationValue : ', err)
            return resolve(false)
          }
          return resolve(user)
        }
      )
    })
  }

}
