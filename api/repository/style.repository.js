/* eslint-disable no-async-promise-executor */
const Style = require('../models/style')
const validation = require('../helpers/validations')
const _ = require('lodash')
const utils = require('../module/utils')
const News = require('../models/news')

module.exports = {

  getStyles: async (onlyName = false) => {
    return new Promise(async (resolve, reject) => {
      try {
        const styles = await Style
          .find()
          .exec()
        return resolve(onlyName ? styles.map(i => i.name) : styles)
      } catch (e) {
        console.error('ERR - allStyles : ', e)
        return reject(new Error('Error getting styles\n' + (e.message || 'no message')))
      }
    })
  },

  /**
         * @param {String} name
         * @param {String} id
         * @returns {Object|String}
         */
  getStyle: async (name = null, id = null) => {
    return new Promise(async (resolve, reject) => {
      let style = null
      try {
        if (!_.isEmpty(name) && typeof name === 'string') {
          style = await Style
            .findOne({ name: utils.capitalize(name) })
            .exec()
        } else if (id && typeof name === 'string' && validation.validateMongooseId(id)) {
          style = await Style
            .findById(id)
            .exec()
        } else {
          return reject(new Error('You have to give a name or an id\ntype: String'))
        }
        return resolve(style)
      } catch (e) {
        console.error('ERR - Get a style : ', e)
        return reject(new Error('error processing the entity Style\n' + (e.message || 'no message')))
      }
    })
  },

  getStyleEmptyObject: () => {
    return new Style()
  },

  /**
         * @param {String} name
         * @returns
         */
  createStyle: async (name) => {
    return new Promise(async (resolve, reject) => {
      try {
        if (!name || typeof name !== 'string') {
          return reject(new Error('name must be a string'))
        }
        const style = await new Style({
          name: utils.capitalize(name),
          slugName: utils.slugify(name)
        })
          .save()
        return resolve(style)
      } catch (e) {
        console.log('ERR - createStyle: ', e)
        return reject(new Error('Error creating style, ' + (e.message || 'no message')))
      }
    })
  },

  /**
         * @param {String} id
         * @returns
         */
  deleteOne: async (id) => {
    return new Promise(async (resolve, reject) => {
      try {
        if (!id && typeof id !== 'string' && !validation.validateMongooseId(id)) { reject(new Error('Invalid or Unknown Id')) }
        await News.findByIdAndDelete(id)
        return resolve(true)
      } catch (e) {
        console.log('ERR - deleteOne: ', e)
        return reject(new Error('Error during deletion: ' + (e.message || 'no message')))
      }
    })
  },

  /**
         * @param {String} style
         * @returns {Promise<String|Boolean>}
         */
  saveStyleAndReturn: async function (style) {
    style = style.split(',')
      .map(st => st.trim()
        .toLowerCase())
      .filter(n => n)

    style = [...new Set(style)]

    console.log('STYLE RECIEVED:: \n', style)

    typeof style !== 'object' && (style = [style])

    const newStyles = await Promise.all(style.map(async (style) => {
      style = utils.slugify(style)
      try {
        if (style && style !== '') {
          const alreadySave = await module.exports.getStyle(style)
          console.log(`Style ${style} is already saved: ${alreadySave && alreadySave._id ? alreadySave._id : false}`)
          if (!alreadySave) {
            const st = await module.exports.createStyle(style)
            return st
          }
          return alreadySave
        }
        return false
      } catch (e) {
        console.error('ERR - Register - save instrument : ', e)
        return false
      }
    })
    )

    const result = await newStyles.filter(n => n)

    return {
      styleIds: result.map(i => i.id),
      styleNames: result.map(i => i.name).join(', ').trim()
    }
  }

}
