
module.exports = {

  getAvatarImageUrl: (gender) => {
    if (gender === 'Women') return '/image/avatar/women.png'
    else if (gender === 'Men') return '/image/avatar/men.png'
    else if (gender === 'Alien') return '/image/avatar/alien.png'
    else if (gender === 'Other') return '/image/avatar/other.png'
    else throw new Error('Gender Value is not supported: ' + gender)
  }

}
