const freeAccess = [
  '/',
  '/app/news',
  '/app/login',
  '/app/register',
  '/app/logout',
  '/about-us'
]

module.exports = freeAccess
