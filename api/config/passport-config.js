const bcrypt = require('bcrypt')
const User = require('../models/user')
const passport = require('passport')
const jwt = require('jsonwebtoken')
const config = require('../config/server')
const LocalStrategy = require('passport-local').Strategy

passport.serializeUser((user, done) => {
  done(null, user.id)
})

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user)
  })
})

passport.use(
  new LocalStrategy({ usernameField: 'username' }, (username, password, done) => {
    User.findOne({ username })
      .then(user => {
        if (!user) {
          return done(null, false, { message: 'No user found..' })
        } else {
          bcrypt.compare(password, user.password, async (err, isSame) => {
            if (err) throw err

            if (isSame) {
              const token = jwt.sign(
                { user_id: user._id },
                config.secret_key,
                { expiresIn: config.timeForSession }
              )

              user.active = true
              user.sessionkey = token

              try {
                await user.save()
              } catch (e) {
                console.log('Err - login user : ', e)
                return done(null, false, { message: 'Error during session-token creation.' })
              }

              return done(null, user)
            } else {
              return done(null, false, { message: "Password doesn't match." })
            }
          })
        }
      })
      .catch((err) => {
        return done(null, false, { message: err })
      })
  })
)

module.exports = passport
