
module.exports = {

  base_url: process.env.BASE_URL || 'http://localhost:1337/',
  database_url: process.env.DATABASE_URL || 'mongodb://localhost/Zicos',
  secret_key: process.env.SECRET_KEY || 'JwtSecretKey',
  session_secret: process.env.SESSION_SECRET || 'SessionSecretKey',
  port: process.env.PORT || 1337,

  timeForSession: '2H',

  ipwhois: {
    url: 'https://ip-geolocation-ipwhois-io.p.rapidapi.com/json/',
    host: 'ip-geolocation-ipwhois-io.p.rapidapi.com',
    key: '4ba5ecf870msh9d42d9581ec0dabp1f2d82jsn7b14a8b0c31b'
  },

  cityLatLng: {
    url: 'https://geocode.maps.co/search?q='
  },

  amperApi: {
    password: '9Un7Q2^%NjYM',
    username: 'bandpromobox@gmail.com',
    baseUrl: 'https://api.ampermusic.com/'
  }
}
