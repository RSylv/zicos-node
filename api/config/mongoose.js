const mongoose = require('mongoose')
const debug = require('debug')

const DEBUG = debug('dev')

const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
}

function connect (DB_URL) {
  mongoose.connect(DB_URL, options)
    .then(() => {
      DEBUG('\x1b[42m\x1b[30m ✅  Mongoose connected !     \x1b[0m\n')
    })
    .catch(logError)
  mongoose.connection.on('error', logError)
}

function logError (err) {
  DEBUG('Mongoose connection error')
  DEBUG(err)
  process.exit(1)
}

module.exports = connect
