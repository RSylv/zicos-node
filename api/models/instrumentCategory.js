const mongoose = require('mongoose')

const instrumentCategorySchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: [true, 'This instrument category already exist.']
    },
    slugName: {
      type: String,
      slug: 'name',
      unique: [true, 'This instrument category already exist.']
    },
    imageUrl: {
      type: String
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('instrumentCategory', instrumentCategorySchema)
