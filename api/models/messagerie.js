const mongoose = require('mongoose')

const messagerieSchema = mongoose.Schema({
  text: {
    type: String,
    required: true
  },
  to: [
    {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    }
  ],
  from: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  read: {
    type: Boolean,
    default: false
  },
  readAt: {
    type: Date,
    default: null
  },
  conversation: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Conversation'
  }
}, { timestamps: true })

const autoPopulateLead = function (next) {
  this.populate('to')
    .populate('from')
  next()
}

messagerieSchema
  .pre('findOne', autoPopulateLead)
  .pre('find', autoPopulateLead)

module.exports = mongoose.model('Messagerie', messagerieSchema)
