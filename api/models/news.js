const mongoose = require('mongoose')

const newsSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true
  },
  titleSlug: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String,
    required: true
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Category'
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
}, { timestamps: true })

module.exports = mongoose.model('News', newsSchema)
