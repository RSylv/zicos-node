const mongoose = require('mongoose')
const News = require('./news')

const categorySchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true
    },
    slugName: {
      type: String,
      required: true,
      unique: true
    },
    description: {
      type: String
    },
    isPublic: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: true
  }
)

categorySchema.pre('remove', function (next) {
  News.find({ category: this.id }, (err, news) => {
    if (err) {
      next(err)
    } else if (news.length > 0) {
      next(new Error('This category has news still'))
    } else {
      next()
    }
  })
})

module.exports = mongoose.model('Category', categorySchema)
