const mongoose = require('mongoose')

const commentSchema = mongoose.Schema({
  message: {
    type: String,
    required: true
  },
  news: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'News'
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  }
}, { timestamps: true })

module.exports = mongoose.model('Comment', commentSchema)
