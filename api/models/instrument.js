const mongoose = require('mongoose')

const instrumentSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: [true, 'This instrument already exist.']
    },
    slugName: {
      type: String,
      slug: 'name',
      unique: [true, 'This instrument already exist.']
    },
    category: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'instrumentCategory'
    },
    mapped: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('Instrument', instrumentSchema)
