const mongoose = require('mongoose')
const slugify = require('slugify')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const geoloc = require('../helpers/geolocation')
const config = require('../config/server')
require('../config/passport-config')

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Username is required'],
    unique: [true, 'This name is already taken.']
  },
  slugName: {
    type: String,
    slug: 'username',
    unique: [true, 'This name is already taken.']
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
    minlength: [4, 'Password should be at least four characters']
    // select: false
  },
  instruments: [
    {
      type: mongoose.Schema.Types.ObjectId,
      // required: [true, "Instrument is required"],
      ref: 'Instrument'
    }
  ],
  instruments_text: {
    type: String
    // required: [true, "Instrument text is required"],
  },
  instrument_image_url: {
    type: String
  },
  want_to_learn: {
    type: String
  },
  avatar_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Avatar'
  },
  avatar_url: {
    type: String
  },
  level: {
    type: String
  },
  style: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Style'
    }
  ],
  style_text: {
    type: String
  },
  place: {
    type: String,
    required: [true, 'Place/City is required']
  },
  latitude: {
    type: Number
  },
  longitude: {
    type: Number
  },
  altitude: {
    type: Number
  },
  accuracy: {
    type: Number
  },
  speed: {
    type: Number
  },
  ip: {
    type: String
  },
  geocodedata: {
    type: String,
    get: function (apidata) {
      try {
        return JSON.parse(apidata)
      } catch (e) {
        return apidata
      }
    },
    set: function (apidata) {
      return JSON.stringify(apidata)
    }
  },
  apidata: {
    type: String,
    get: function (apidata) {
      try {
        return JSON.parse(apidata)
      } catch (e) {
        return apidata
      }
    },
    set: function (apidata) {
      return JSON.stringify(apidata)
    }
  },
  active: {
    type: Boolean
  },
  email: {
    type: String
  },
  salt: {
    type: String
  },
  hash: {
    type: String
  },
  sessionkey: {
    type: String
  },
  conversations: [
    {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Conversation'
    }
  ],
  update_location: {
    type: Boolean
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
}, { timestamps: true })

UserSchema.methods.generateAuthToken = async () => {
  try {
    console.log('ID : ', this.id)
    const token = jwt.sign({ _id: this.id.toString() }, config.secret_key)
    this.tokens = this.tokens.concat({ token })
    await this.save()
    return token
  } catch (e) {
    console.log('ERR - generateAuthToken: ', e)
  }
}

UserSchema.methods.validPassword = (password) => {
  return bcrypt.compareSync(password, this.password)
}

UserSchema.method.generateVerificationToken = function () {
  return jwt.sign({ id: this._id }, config.secret_key, {
    expiresIn: '10d',
    algorithm: 'RS256'
  })
}

UserSchema.statics.checkExistingField = async (field, value) => {
  const checkField = await mongoose.model('User', UserSchema).findOne({ [`${field}`]: value }).exec()
  return checkField
}

UserSchema.pre('save', async function (next) {
  const data = await geoloc.getLatLng(this.place)
  if (data) {
    this.geocodedata = data
    this.latitude = data.lat
    this.longitude = data.lon
  }
  this.slugName = slugify(this.username)
  next()
})

UserSchema.pre('findOne', function (next) {
  this.populate('style')
    .populate('instruments')
  next()
})

UserSchema.index({ username: 'text', slugName: 'text', place: 'text', style_text: 'text', instruments_text: 'text' })

module.exports = mongoose.model('User', UserSchema)
