const mongoose = require('mongoose')
const User = require('./user')

const avatarSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true
    },
    path: {
      type: String,
      required: true,
      unique: true
    },
    gender: {
      type: String
    }
  },
  {
    timestamps: true
  }
)

avatarSchema.pre('remove', function (next) {
  User.find({ avatar_id: this.id }, (err, avatar) => {
    if (err) {
      next(err)
    } else if (avatar.length > 0) {
      next(new Error('This category has avatar still'))
    } else {
      next()
    }
  })
})

module.exports = mongoose.model('Avatar', avatarSchema)
