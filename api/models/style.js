const mongoose = require('mongoose')

const styleSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: [true, 'This style already exist.']
    },
    slugName: {
      type: String,
      slug: 'name',
      unique: [true, 'This style already exist.']
    }
  },
  {
    timestamps: true
  }
)

module.exports = mongoose.model('Style', styleSchema)
