const mongoose = require('mongoose')
const Messagerie = require('./messagerie.js')

const conversationSchema = mongoose.Schema(
  {
    groupName: {
      type: String,
      default: null
    },
    lastWriter: {
      type: String,
      default: 'Jean-Claude Parpaing'
    },
    messages: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Messagerie'
      }
    ],
    users: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      }
    ]
  },
  { timestamps: true }
)

conversationSchema.pre('findOne', function (next) {
  this.populate('messages')
  next()
})

conversationSchema.pre('remove', function (next) {
  Messagerie.deleteMany({ conversation: this._id }).exec()
  next()
})

module.exports = mongoose.model('Conversation', conversationSchema)
