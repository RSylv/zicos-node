/* eslint-disable camelcase */
const _ = require('lodash')
const freeAcess = require('../config/free_access')
const conversationRepository = require('../repository/conversation.repository')

module.exports = {
  redirections: async (req, res, next) => {
    console.log('FROM-URL: ', req.app.locals.from_url)
    console.log('ReqUrl: ', req.url + '\nFree Access Route: ', _.includes(freeAcess, req.url) + '\nCONNECTED:', req.isAuthenticated())

    if (!_.includes(freeAcess, req.url)) {
      console.log('Passport Session: ', req.session)

      if (!req.user) { return res.status(401).redirect('/app/logout') }

      console.log('User: ', { id: req.user.id })
    }

    const no_back_url = ['/app/login', 'app/register']

    req.back_url = req.app.locals.from_url
    req.app.locals.from_url = !no_back_url.includes(req.url) ? req.url : null

    if (req.user) {
      const tempArray = []
      const conversations = await conversationRepository.getAllUserConversations(req.user)
      if (conversations && conversations.length) {
        conversations.forEach(conv => {
          if (conv.messages && conv.messages.length) {
            for (let i = 0; i < conv.messages.length; i++) {
              if (!conv.messages[i].read) {
                tempArray.push(conv.messages[i].to.map(u => u.id).some(m => req.user.id === m))
              }
            }
          }
        })
      }
      req.haveNewMessage = tempArray.some(msg => msg === true)
    }

    next()
  }
}
