function validateSession (req, res, next) {
  console.log('\nVALIDATE SESSION MIDDLEWARE\n')
  if (!req.session.user && req.userId !== req.session.user.id) {
    return res.status(403).redirect('/logout')
  }
  next()
}

module.exports = validateSession
