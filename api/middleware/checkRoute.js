module.exports = {
  /**
   * If a user is logged in, we continue
   * Otherwise it is redirected to the login page
   */
  checkAuthenticated: (req, res, next) => {
    console.log('checkAuthenticated: ', req.isAuthenticated())
    if (req.isAuthenticated()) {
      next()
    } else {
      return res.redirect('/login')
    }
  },

  /**
   * If a user is logged in, we may want to block some access (register, login, (..))
   */
  checkNotAuthenticated: (req, res, next) => {
    console.log('checkNotAuthenticated: ', req.isAuthenticated())
    if (!req.isAuthenticated()) {
      next()
    } else {
      return res.redirect('/')
    }
  }
}
