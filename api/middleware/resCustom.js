function resCustom (req, res, params, toJson = false) {
  console.log(params)
  req.user && (params.user = req.user)
  if (params.redirectTo) {
    return res.redirect(params.redirectTo)
  }
  return res.render(params.path, params.obj)
}

module.exports = resCustom
