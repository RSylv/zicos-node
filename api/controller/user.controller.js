const userRepository = require('../repository/user.repository')
const utils = require('../module/utils')

module.exports = {

  getPanel: (req, res) => {
    res.render('admin/panel', {
      is_authenticated: req.isAuthenticated(),
      user: req.user,
      page_title: 'Administration',
      back_url: req.back_url,
      haveNewMessage: req.haveNewMessage
    })
  },

  getUserList: async (req, res, next) => {
    const users = await userRepository.getAllUser()
    res.render('admin/users', {
      is_authenticated: req.isAuthenticated(),
      user: req.user,
      users: users && users.length ? users : [],
      page_title: 'Admin - Users',
      back_url: req.back_url,
      haveNewMessage: req.haveNewMessage
    })
  },

  getUser: async (req, res, next) => {
    if (!utils.isValidMongoId(req.params.id)) {
      throw new Error('Invalid ID')
    }
    if (
      !req.user ||
      !req.user._id ||
      (!req.user.isAdmin && req.params.id !== req.user._id)
    ) {
      res.redirect('/')
    }
    const user = await userRepository.getUser(req.params.id)
    res.render('admin/user', {
      is_authenticated: req.isAuthenticated(),
      user: user || null,
      page_title: 'Admin - User',
      back_url: req.back_url,
      haveNewMessage: req.haveNewMessage
    })
  },

  deleteUser: async (req, res, next) => {
    const _id = req.body.user_id
    console.log('ID:', _id)
    if (!req.body.id && !utils.isValidMongoId(_id)) {
      throw new Error('Invalid ID')
    }
    if (!await userRepository.deleteUser(_id)) {
      throw new Error('No user found')
    }
    res.redirect('user/all')
  },

  updateUserLocationValue: async (req, res, next) => {
    const locationData = {
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      altitude: req.body.altitude,
      accuracy: req.body.accuracy,
      speed: req.body.speed
    }
    try {
      await userRepository.updateUserPosition(
        req.user.id,
        locationData
      )
      return res.send('ok')
    } catch (e) {
      console.log(e)
      return res.send(false)
    }
  }

}
