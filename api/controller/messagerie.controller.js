/* eslint-disable camelcase */
const utils = require('../module/utils')
const Messagerie = require('../models/messagerie')
const User = require('../models/user')
const userRepository = require('../repository/user.repository')
const Conversation = require('../models/conversation')
const conversationRepository = require('../repository/conversation.repository')

module.exports = {

  messagerie: async (req, res) => {
    try {
      console.log('haveNewMessage: ', req.haveNewMessage)

      const conversations = await conversationRepository.getAllUserConversations(req.user)
      if (!conversations || !conversations.length) {
        return res.render('messagerie/messages', {
          is_authenticated: req.isAuthenticated(),
          user: req.user,
          conversations: [],
          page_title: 'Messagerie',
          back_url: req.back_url,
          haveNewMessage: req.haveNewMessage
        })
      }

      conversations.sort((a, b) => b.createdAt - a.createdAt)

      return res.render('messagerie/messages', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        conversations,
        page_title: 'Messagerie',
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('ERR - Messagerie : ', e)

      return res.render('500', { error: e.message })
    }
  },

  getNew: async (req, res) => {
    console.log('-----> req.params.conversationId: ', req.params.conversationId)

    let requestedUser = null

    let data
    if (req.query.username) {
      requestedUser = await userRepository.getUserByUsername(req.query.username)
      if (!requestedUser) {
        return res.redirect('/app/messagerie')
      }
      data = {}
      data.message_to = req.query.username
    }
    console.log('DATA:', data)

    const alreadyConverse = requestedUser
      ? await Messagerie.findOne({
        $or: [{ to: requestedUser.id, from: req.user.id }, { from: requestedUser.id, to: req.user.id }]
      })
      : false
    console.log('ALREADY_CONVERSE: ', !!alreadyConverse)
    if (alreadyConverse) {
      return res.redirect(`/app/messagerie/${alreadyConverse.conversation}`)
    }

    return res.render('messagerie/new', {
      is_authenticated: req.isAuthenticated(),
      user: req.user,
      conversation: null,
      page_title: 'Conversation',
      back_url: req.back_url,
      data,
      haveNewMessage: req.haveNewMessage
    })
  },

  getOne: async (req, res) => {
    try {
      console.log('Param: ', req.params.id)
      const conversation = await Conversation.findById(req.params.id).populate('messages').populate('users', 'username')
      if (!conversation) {
        return res.redirect('/app/messagerie')
      }

      await Messagerie.updateMany({ conversation: conversation.id, to: req.user }, { read: true, readAt: new Date() })

      conversation.messages = conversation.messages.sort((a, b) => b.createdAt - a.createdAt)
      console.log(
        '**********\n',
        { page_title: `${conversation.groupName || 'Conversation'}` },
        '\n**********'
      )
      return res.render('messagerie/conversation', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        conversation,
        page_title: `${conversation.groupName || 'Conversation'}`,
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('ERR - message - getOne : ', e)

      return res.render('500', { error: e.message })
    }
  },

  postNew: async (req, res) => {
    try {
      const errorResponse = {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        data: req.body,
        conversations: [],
        page_title: 'Messagerie',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      }
      if (!req.body.message_to || !req.body.text) {
        errorResponse.error = 'Vous devez remplir les champs "A" et "Message"'
        return res.render('messagerie/new', errorResponse)
      }
      // Get the user To send the message
      const toUser = await User.findOne({ username: req.body.message_to.trim() }).exec()
      if (!toUser) {
        console.log('------\n Out From "!toUser"... \n------')
        errorResponse.error = `Aucun utilisateur trouvé avec ce pseudo: "${req.body.message_to}"`
        return res.render('messagerie/new', errorResponse)
      }

      // Get or Create the conversation
      let { conversation, isNew } = await conversationRepository.getOrCreateConversationObject(req.body.conversation, req.body.group_name)
      console.log('getOrCreateConversationObject: ', { conversation, isNew })

      // Add users to the conversation If not exist
      conversation = conversationRepository.addUsersToConversation(conversation, toUser, req.user)

      // Create and Save the message
      let newMessage = new Messagerie({
        to: toUser,
        from: req.user,
        text: req.body.text.trim(),
        conversation: conversation._id
      })
      newMessage = await newMessage.save()

      // Add the message to the conversation, and save it
      const savedConversation = await conversationRepository.addMessageToConversationAndSave(conversation, newMessage, isNew)

      // Get All user conversations
      const user = await User.findById(req.user.id)
      let allConversation = await conversationRepository.getAllUserConversations(user, savedConversation)

      await conversationRepository.updateUserConversations(toUser, user, savedConversation)

      // PopulateAll in allConversation
      allConversation = await utils.autoPopulateObject(allConversation)
      console.log('allConversation: ', allConversation)

      return res.redirect(`/app/messagerie/${savedConversation.id}`)
    } catch (e) {
      console.log('ERR - new message : ', e)

      return res.render('500', { error: e.message })
    }
  },

  postConversationMessage: async (req, res) => {
    try {
      // TODO: send conversation ID Hash in getOne & get the deHash here
      console.log('ConversationId-Hidden-Form :: ', req.body.conversation_id)
      const conversation = await Conversation.findById(req.body.conversation_id).populate('users', 'id')

      const toUsers = conversation.users.map(u => req.user.id !== u.id && u.id).filter(n => n)

      let newMessage = new Messagerie({
        to: toUsers,
        from: req.user,
        text: req.body.text.trim(),
        conversation: conversation._id
      })
      newMessage = await newMessage.save()
      conversation.lastWriter = req.user.username

      const savedConversation = await conversationRepository.addMessageToConversationAndSave(conversation, newMessage)

      savedConversation.messages = savedConversation.messages.sort((a, b) => b.createdAt - a.createdAt)

      console.log('savedConversation: ', savedConversation)

      return res.render('messagerie/conversation', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        conversation: savedConversation,
        page_title: `${savedConversation.groupName || 'Conversation'}`,
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('ERR - post Conversation Message : ', e)

      return res.render('500', { error: e.message })
    }
  },

  deleteConversation: async (req, res) => {
    try {
      const conversation_id = req.body.conversation_id.trim()
      console.log('in::', conversation_id)
      const conv = await Conversation.findById(conversation_id)
      console.log('CONV:::', conv)
      if (!conv) {
        return res.redirect('/app/messagerie')
      }

      await conversationRepository.deleteUserConversation(req.user, conversation_id)

      console.log('REMOVE...')
      const r = await conv.remove()
      console.log('REMOVED::', r)

      const conversations = await conversationRepository.getAllUserConversations(req.user)
      if (!conversations || !conversations.length) {
        return res.render('messagerie/messages', {
          is_authenticated: req.isAuthenticated(),
          user: req.user,
          conversations: [],
          page_title: 'Message',
          haveNewMessage: req.haveNewMessage
        })
      }
      return res.render('messagerie/messages', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        conversations,
        page_title: 'Message',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('ERR - deleteConversation :', e)

      return res.render('500', { error: e.message })
    }
  }

}
