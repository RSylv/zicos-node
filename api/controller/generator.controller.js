/* eslint-disable camelcase */
const User = require('../models/user')
const Style = require('../models/style')
const InstrumentModel = require('../models/instrument')
const geoloc = require('../helpers/geolocation')
const instrumentRepository = require('../repository/instrument.repository')
const utils = require('../module/utils')
const bcrypt = require('bcryptjs')
const axios = require('axios')
const _ = require('lodash')

// Datas
const instruments = ['guitare', 'basse', 'piano', 'harpe', 'accordeon', 'ukulele', 'cuivre', 'batterie']
const avatars = utils.getAllFileNameInPath('/public/image/animals/')
const levels = ['Beginner', 'Amateur', 'Pro']
const comments = [
  "J'adore le sky, et le bacon. Je cherche a apprendre le oud ... Si quelqu'un peut m'aider ! mp ;)",
  "Bonjour la communautée :) Je joue de la basse dans un groupe de metal, mais j'aimerais apprendre le piano.. un douc rêve !",
  "Salut, je serais trop content si j'arrivais a me faire des potes ici, je suis batteur",
  "Qui pourrais m'aider a avancer plus vite sur mes cours de guitare ? J'ai bientôt 16ans, je veux choper mais j'ai pas le niveau requis",
  "J'ém bi1 le con7pt, je cherch a perfom mon nivo de flute, desja balaiz mé bon je sui pas contre evol,.. enfin voala koi",
  'Messieurs, je cherche a jouer avec un ensemble de stilldrum. Je suis souvent a moitié nue.. :)',
  "J'aime le sport, macron, un peu le fn. Je cherche a monter un vrai groupe de droite qui assume son coté artistique et créatif ! A bon(ne) entendeu(r(se)",
  'Je kiffe le scratch et le mix... mais je suis mauvais',
  "Mes parents sont blindé de tunes (pour acheter du matos, nous trimballer tout ca !!) mais je ne sais pas bien jouer.. je cherche des musiciens pro pour un projet d'envergure",
  '... RAS ...',
  "Only rock'n roll ou Punk.. limite metal, mais j'aime pas trop la zic de pede"
]
const cities = []
let USERS_LENGTH = 100
let USERNAMES = []
let index = 0

function getSavedUser () {
  const offset = Math.floor(Math.random() * 20000)
  axios.get(`http://geodb-free-service.wirefreethought.com/v1/geo/cities?limit=1&offset=${offset}&hateoasMode=off`)
    .then(function (response) {
      const city = response.data.data[0].city
      console.log(offset, city)
      cities.push(city)

      getRandomBaseUserDataSet(city)
        .then(userData => {
          new User(userData)
            .save()
            .then(user => {
              index++
              console.log('Index: ', index, '\nUSERS_LENGTH: ', USERS_LENGTH)
              console.log('Username: ', user.username)
              console.log('END', !(index < USERS_LENGTH))
              if (index < USERS_LENGTH) { getSavedUser() }
            })
            .catch(err => {
              throw new Error(err)
            })
        })
        .catch(err => {
          throw new Error(err.message || err)
        })
    })
    .catch(function (err) {
      console.log('ERROR - API - Cities :', err)
    })
}
async function getRandomBaseUserDataSet (city, username) {
  try {
    const username = getRandomUsername()
    const password = bcrypt.hashSync(username, bcrypt.genSaltSync(10))
    const instrument = getRandomInstruments(true)
    const style = await getRandomStyles(true)
    const instrIds = await getInstrumentsId(instrument)
    const apiRes = await geoloc.getFormattedPositionIp()
    const avatar_url = avatars[Math.floor(Math.random() * avatars.length)]

    return {
      username,
      password,
      email: `${username}@gmail.com`,
      place: city,
      avatar_url,
      level: levels[Math.floor(Math.random() * levels.length)],
      instruments_text: instrument.join(', '),
      instruments: instrIds,
      instrument_image_url: instrumentRepository.getInstrumentImageUrl(instrument.join(',')),
      style: style.id,
      style_text: style.name.toLowerCase(),
      ip: apiRes.ip,
      apidata: _.omit(apiRes, ['ip', 'latitude', 'longitude']),
      want_to_learn: comments[Math.floor(Math.random() * comments.length)]
    }
  } catch (e) {
    throw new Error('ERR - getRandomBaseUserDataSet:\n' + e.message || e)
  }
}

function getRandomUsername () {
  if (!USERNAMES || !USERNAMES.length) { throw new Error('Usernames array is empty') }
  return USERNAMES.splice(Math.floor(Math.random() * USERNAMES.length), 1).toString()
}
function getRandomInstruments (toArray = false) {
  const LEN = Math.floor(Math.random() * 5) + 1
  const res = []
  for (let i = 0; i < LEN; i++) {
    res.push(instruments[Math.floor(Math.random() * instruments.length)])
  }
  if (toArray) {
    return [...new Set(res)]
  }
  return [...new Set(res)].join(', ')
}
async function getRandomStyles () {
  try {
    const styles = await Style.find().sort('-name').exec()
    return styles[Math.floor(Math.random() * styles.length)]
  } catch (e) {
    console.log('ERR: ', e)
  }
}
async function getInstrumentsId (names) {
  const res = []
  try {
    const instrs = await InstrumentModel.find().sort('-name').exec()
    instrs.forEach(instr => {
      names.forEach(name => {
        instr.name === name && res.push(instr.id)
      })
    })
    return res
  } catch (e) {
    console.log('ERR: ', e)
  }
}

function initialize (number = 0) {
  axios.get('https://random-word-api.herokuapp.com/all')
    .then(function (response) {
      console.log('-- API NAMES RES OK --')
      USERNAMES = response.data
      console.log(`-- LENGTH: ${USERNAMES.length} --`)
      USERS_LENGTH = typeof number === 'number' && number > 0 && number < 100 ? number : USERS_LENGTH
      getSavedUser()
    })
    .catch(function (err) {
      console.log('ERROR - API - Names :', err.message || err)
      throw new Error('ERROR - API - Names :\n' + err.message || err)
    })
}

module.exports = {
  generateUser: initialize
}
