const User = require('../models/user')
const userRepository = require('../repository/user.repository')
const instrumentRepository = require('../repository/instrument.repository')
const styleRepository = require('../repository/style.repository')
const utils = require('../module/utils')

module.exports = {
  myAccount: async (req, res) => {
    const instruments = await instrumentRepository.getInstruments()
    const styles = await styleRepository.getStyles()
    const filenames = utils.getAllFileNameInPath('/public/image/animals/')
    res.render('account/my-account', {
      is_authenticated: req.isAuthenticated(),
      user: req.user,
      page_title: 'Mon Compte',
      back_url: req.back_url,
      instruments: JSON.stringify(instruments.map(i => i.name)),
      styles: JSON.stringify(styles.map(i => i.name)),
      filenames: utils.shuffle(filenames),
      haveNewMessage: req.haveNewMessage
    })
  },

  userDetails: async (req, res) => {
    if (!req.params.username) { res.redirect('account/user-details') }
    const user = await User.findOne({ username: req.params.username })
    res.render('account/user-details', {
      is_authenticated: req.isAuthenticated(),
      page_title: user.username,
      back_url: req.back_url,
      user,
      haveNewMessage: req.haveNewMessage
    })
  },

  list: async (req, res) => {
    try {
      const escapeStr = req.query.users ? req.query.users.replace(/[^a-zA-ZÀ-ÖØ-öø-ÿ0-9@. -]+/g, '').trim().toString() : null
      const cleanedSearch = escapeStr ? new RegExp(escapeStr) : null
      const users = await userRepository.getUserBySearchTerm(cleanedSearch)
      res.render('account/userlist', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        users,
        page_title: 'Les utilisateurs',
        back_url: req.back_url,
        search_terms: escapeStr,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('Err - list users : ', e)
    }
  }

}
