const instrumentMapper = require('../helpers/instrument-mapper')
const Category = require('../models/category.js')
const utils = require('../module/utils')

module.exports = {
  saveInstrumentCategoriesOnDB: async (req, res) => {
    console.log('IN => saveInstrumentCategoriesOnDB')
    try {
      const result = await instrumentMapper.saveInstrumentCategoryOnDB()
      return res.status(200).send(result)
    } catch (error) {
      console.log('Err - ADMIN - saveInstrumentCategoriesOnDB: ', error)
      res.redirect('/')
    }
  },

  saveInstrumentOnDB: async (req, res, next) => {
    try {
      const result = await instrumentMapper.saveInstrumentOnDB()
      return res.status(200).send(result)
    } catch (error) {
      console.log('Err - ADMIN - saveInstrumentOnDB: ', error)
      res.redirect('/')
    }
  },

  getAllNewsCategories: async (req, res, next) => {
    try {
      const categories = await Category.find({})
      res.render('admin/categories', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        categories: categories && categories.length ? categories : [],
        page_title: 'Admin - Categories',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
      return categories
    } catch (error) {
      console.log('Err - ADMIN - getAllNewsCategories: ', error)
      res.redirect('/')
    }
  },

  createCategory: async (req, res) => {
    try {
      console.log('----- IN -----', req.body.cat_is_public)
      const cat = await Category.create({
        name: req.body.cat_name,
        slugName: utils.slugify(req.body.cat_name),
        description: req.body.cat_description,
        isPublic: req.body.cat_is_public === 'on'
      })
      console.log(cat)
      res.redirect('/administration/categories')
    } catch (error) {
      console.log('Err - ADMIN - createCategory: ', error)
      res.redirect('/')
    }
  },

  deleteCategory: async (req, res, next) => {
    if (
      !req.params.id &&
      !utils.isValidMongoId(req.params.id)
    ) {
      res.redirect('/')
    }
    try {
      await Category.deleteOne({ id: req.params.id })
      res.redirect('/administration/categories')
    } catch (error) {
      console.log('Err - ADMIN - deleteCategory: ', error)
      res.redirect('/')
    }
  }

}
