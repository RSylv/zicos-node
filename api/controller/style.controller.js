const Style = require('../models/style')
const utils = require('../module/utils')
const slugify = require('slugify')

module.exports = {
  allStyles: async (req, res) => {
    try {
      const styles = await Style.find().sort('-name').exec()

      return res.render('style/index', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        styles,
        page_title: 'Styles',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('Err - allStyles : ', e)

      return res.render('500', { error: e.message })
    }
  },

  getNew: (req, res) => {
    return res.render('style/new', {
      is_authenticated: req.isAuthenticated(),
      user: req.user,
      style: new Style(),
      page_title: 'Add Style',
      back_url: req.back_url,
      haveNewMessage: req.haveNewMessage
    })
  },

  postNew: async (req, res) => {
    try {
      const styleName = req.body.name && utils.capitalize(req.body.name)
      if (!styleName) {
        return res.render('style/new', {
          is_authenticated: req.isAuthenticated(),
          user: req.user,
          style: new Style(),
          error: 'Unvalid style name. Only Letters',
          page_title: 'Add Style',
          haveNewMessage: req.haveNewMessage
        })
      }
      const style = new Style({
        name: styleName,
        slugName: slugify(styleName)
      })
      await style.save()
      const styles = await Style.find()
      return res.render('style/index', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        styles,
        page_title: 'Styles',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.error('Err - styles - postNew : ', e)

      let errorMessage = 'Error during processing'

      if (e.message && e.message.startsWith('E11000 duplicate')) { errorMessage = 'This Style already exists' }

      return res.render('style/new', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        style: new Style(),
        error: errorMessage,
        page_title: 'Add Style',
        haveNewMessage: req.haveNewMessage
      })
    }
  },

  getOne: async (req, res) => {
    // TODO:
  }
}
