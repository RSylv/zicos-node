/* eslint-disable camelcase */
const slugify = require('slugify')
const getDateAndTime = require('../module/utils').getDateAndTime
const News = require('../models/news')
const Category = require('../models/category.js')
const Comment = require('../models/comment.js')

module.exports = {
  /**
   * GET
   * Get all the news
   */
  allNews: async (req, res) => {
    try {
      // Get news with category name, sorted and limited by 10.
      const news = await News
        .find()
        .populate('category')
        .sort({ createdAt: 'desc' })
        .limit(10)
        .exec()

      // Add comments number if news exists
      if (news && news.length) {
        for (let i = 0; i < news.length; i++) {
          const comm = await Comment.find({ news: news[i]._id })
          news[i].comments_number = comm && comm.length ? comm.length : 0
        }
      }

      return res.render('news/index', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        news: news && news.length ? news : [],
        page_title: 'News',
        sub_header_text: 'All the News !',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (error) {
      console.error('Err - allNews : ', error)
      res.render('news/new', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        page_title: 'News',
        news: {},
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage,
        errorMessage: 'Error getting News'
      })
    }
  },

  /**
   * GET
   * Get one news.
   */
  oneNews: async (req, res) => {
    try {
      const news = await News
        .findOne({ titleSlug: req.params.slug })
        .populate('category')
        .populate('user')
        .exec()

      const comments = await Comment.find({ news: news._id }).populate('user')
      if (comments && comments.length) {
        comments.forEach((com, i) => {
          const { date, time } = getDateAndTime(com.createdAt)
          comments[i].date = date
          comments[i].time = time
        })
      }

      // Format date object
      const { date, time } = getDateAndTime(news.createdAt, news.updatedAt)
      news.date = date
      news.time = time

      return res.render('news/single', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        news,
        comments: comments && comments.length ? comments : [],
        page_title: 'News',
        sub_header_text: news.title,
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (error) {
      console.error('Err - oneNews : ', error)
      return res.redirect('/app/news')
    }
  },

  /**
   * GET
   * Get the add news
   */
  getAddNews: async (req, res) => {
    try {
      let categories = []
      if (req.user.isAdmin === true) {
        categories = await Category.find()
      } else {
        categories = await Category.find({ isPublic: true })
      }
      res.render('news/add', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        news: new News(),
        categories: categories && categories.length ? categories : [],
        page_title: 'Add News',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.error('Err - getAddNews : ', e)
      res.redirect('/')
    }
  },

  /**
   * POST
   * Create a news
   */
  createNews: async (req, res) => {
    try {
      // Check for userId
      if (!(req.user && req.user._id)) {
        return res.redirect('/login')
      }

      // slug Title & format Content
      const slug_title = slugify(req.body.title, { remove: /[*+~.()'"!:@<>]/g, lower: true, strict: true })
      const text = req.body.content.trim().replace(/(\r\n|\n|\r)/g, '</p><p>')
      const content = '<p>' + text + '</p>'

      // get Category
      const category = await Category
        .findOne({ _id: req.body.category })
        .exec()

      // create & save
      const news = await new News({
        title: req.body.title,
        titleSlug: slug_title,
        content,
        imageUrl: req.body.image_url,
        category,
        user: req.user,
        back_url: req.back_url
      }).save()

      return res.redirect(`/app/news/${news.titleSlug}`)
    } catch (error) {
      console.log('Err - createNews: ', error)

      return res.redirect('/app/news')
    }
  },

  /**
   * DELETE
   * Delete a category
   */
  deleteNews: async (req, res, next) => {
    try {
      if (
        !req.params.id ||
        typeof req.params.id !== 'string'
      ) {
        return res.status(422).redirect('/news')
      }

      await News.findByIdAndDelete(req.params.id)
      return res.status(200).render('news', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        page_title: 'News',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('ERR - createNews: ', e)

      res.status(500).render('news/new', {
        news: `${req.params.id}`,
        errorMessage: 'Error deleting news'
      })
    }
  },

  addComment: async (req, res, next) => {
    try {
      // Check for userId & Comment data
      if (!(req.user && req.user._id)) {
        return res.redirect('/login')
      }
      if (!req.body.news_id || !req.body.comment_content) {
        return res.redirect('/app/news')
      }

      // Format the message
      const text = req.body.comment_content.trim().replace(/(\r\n|\n|\r)/g, '</p><p>')
      const message = '<p>' + text + '</p>'

      // Get the news Or redirect
      const news = await News.findById(req.body.news_id)
      if (!news) {
        return res.redirect('/app/news')
      }

      await new Comment({
        user: req.user._id,
        message,
        news
      }).save()

      return res.redirect(`/app/news/${news.titleSlug}`)
    } catch (error) {
      console.log('ERR - addComment: ', error)

      return res.redirect('/app/news')
    }
  }
}
