const User = require('../models/user')

module.exports = {
  getMap: async (req, res) => {
    console.log('QUERY: ', req.query)
    try {
      // const location = await geolocation.getFormattedPosition()

      const users = await User.find()
        .select('-password -createdAt -updatedAt -style -ip -apidata')
        .populate({
          path: 'instruments',
          populate: 'category'
        })

      let userSelected = null
      if (req.query.lat && req.query.lon) {
        userSelected = [req.query.lat, req.query.lon]
      }
      console.log(userSelected)
      const dataJson = JSON.stringify({ users, userSelected })

      return res.render('map/geoloc', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        dataJson,
        page_title: 'La carte',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.error('ERR - Geoloc : ', e)

      return res.render('500', { error: e.message })
    }
  }

}
