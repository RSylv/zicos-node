const bcrypt = require('bcryptjs')
const userRepository = require('../repository/user.repository')
const User = require('../models/user')
const utils = require('../module/utils')
const instrumentRepository = require('../repository/instrument.repository')
const styleRepository = require('../repository/style.repository')
const geoloc = require('../helpers/geolocation')
const _ = require('lodash')
const geolocation = require('../helpers/geolocation')

module.exports = {

  getRegister: async (req, res) => {
    try {
      const instruments = await instrumentRepository.getInstruments()
      const styles = await styleRepository.getStyles()
      const filenames = utils.getAllFileNameInPath('/public/image/animals/')

      return res.render('auth/register', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        instruments: JSON.stringify(instruments.map(i => i.name)),
        styles: JSON.stringify(styles.map(i => i.name)),
        data: null,
        error: null,
        filenames: utils.shuffle(filenames),
        page_title: 'Inscription',
        sub_header_text: 'Create Your Account !',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.error('ERR - GET Registration : ', e)
    }
  },

  register: async (req, res) => {
    try {
      const data = req.body
      const filenames = utils.getAllFileNameInPath('/public/image/animals/')
      const instruments = await instrumentRepository.getInstruments()
      const styles = await styleRepository.getStyles()

      console.log('IN: ', data)

      console.log('********** \n', data.formtype, '\n**********')

      if (!data?.formtype || data.formtype !== 'create') {
        console.log('ERROR:: \ndata.formtype is missing or not a valid form type\n', {
          formtype: data?.formtype
        })
        return res.redirect('/app/logout')
      }

      // Validations
      if (!_.every(data, String)) {
        return res.status(422).render('auth/register', {
          s_authenticated: req.isAuthenticated(),
          error: 'Empty values, or only String.',
          data,
          filenames: utils.shuffle(filenames),
          instruments: JSON.stringify(instruments.map(i => i.name)),
          styles: JSON.stringify(styles.map(i => i.name)),
          page_title: 'Register',
          haveNewMessage: req.haveNewMessage
        })
      }

      const userAlreadyExist = await userRepository.getUserByUsername(data.user_pseudo)
      if (userAlreadyExist) {
        return res.status(400).render('auth/register', {
          is_authenticated: req.isAuthenticated(),
          error: 'This pseudo already exist.',
          data,
          filenames: utils.shuffle(filenames),
          instruments: JSON.stringify(instruments.map(i => i.name)),
          styles: JSON.stringify(styles.map(i => i.name)),
          page_title: 'Register',
          haveNewMessage: req.haveNewMessage
        })
      }

      if (data.user_password !== data.user_confirmPassword) {
        return res.status(400).render('auth/register', {
          is_authenticated: req.isAuthenticated(),
          error: 'Password are not the same.',
          data,
          filenames: utils.shuffle(filenames),
          instruments: JSON.stringify(instruments.map(i => i.name)),
          styles: JSON.stringify(styles.map(i => i.name)),
          page_title: 'Register',
          haveNewMessage: req.haveNewMessage
        })
      }

      const password = await bcrypt.hash(data.user_password, bcrypt.genSaltSync(10))
      const apiRes = await geoloc.getFormattedPositionIp()

      const user = await userRepository.createUser({
        username: data.user_pseudo,
        password,
        place: data.user_place,
        ip: apiRes.ip,
        apidata: _.omit(apiRes, ['ip', 'latitude', 'longitude'])
      })

      return res.status(201).render('index', {
        is_authenticated: false,
        user,
        page_title: 'Home',
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('Err - register : ', e)

      const msg = utils.getMongoError(e)

      return res.render('500', { error: msg || e.message })
    }
  },

  updateUser: async (req, res, next) => {
    try {
      const data = req.body
      const user = await userRepository.getUser(req.user.id)
      const apiRes = await geolocation.getFormattedPositionIp()

      console.log('********** \n', data.formtype, '\n**********')

      if (!data?.formtype || data.formtype !== 'update' || !req.user?.id) {
        console.log('ERROR:: \ndata.formtype is missing or not a valid form type, or req.user.id is undefined\n', {
          formtype: data?.formtype,
          id: req.user?.id
        })
        return res.redirect('/app/logout')
      }

      const { instrIds, instrNames } = await instrumentRepository.saveInstrumentsAndReturn(data.user_instruments)
      const { styleIds, styleNames } = await styleRepository.saveStyleAndReturn(data.user_styles)

      user.email = data.user_email
      user.place = data.user_place
      user.avatar_url = data.user_avatar_url
      user.level = data.user_level
      user.instruments = instrIds
      user.instruments_text = instrNames
      user.instrument_image_url = instrumentRepository.getInstrumentImageUrl(instrNames)
      user.style = styleIds
      user.style_text = styleNames
      user.want_to_learn = data.want_to_learn.trim()
      user.ip = apiRes.ip
      user.apidata = _.omit(apiRes, ['ip', 'latitude', 'longitude'])

      await userRepository.updateOne(user)

      const filenames = utils.getAllFileNameInPath('/public/image/animals/')
      return res.render('account/my-account', {
        is_authenticated: req.isAuthenticated(),
        user,
        page_title: 'Mon Compte',
        back_url: req.back_url,
        instruments: JSON.stringify(await instrumentRepository.getInstruments(true)),
        styles: JSON.stringify(await styleRepository.getStyles(true)),
        filenames: utils.shuffle(filenames),
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('Err - updateUser : ', e)

      const msg = utils.getMongoError(e)
      console.log('MongoError: ', msg)

      return res.render('500', {
        page_title: 'error',
        is_authenticated: req.isAuthenticated()
      })
    }
  },

  getLogin: (req, res) => {
    console.log('User: ', req.user)
    console.log('Session: ', req.session)
    return res.render('auth/login', {
      is_authenticated: req.isAuthenticated(),
      user: req.user,
      flashmessage: req.flash('error'),
      page_title: 'Connexion',
      back_url: req.back_url,
      haveNewMessage: req.haveNewMessage
    })
  },

  logUser: async (req, res) => {
    delete req.user.sessionkey
    console.log('||-----> OK !', req.isAuthenticated())
    console.log('User: ', { user: req.user._id })

    return res.redirect('/app/account')
  },

  isUserLoggedIn: async (req, res, next) => {
    if (req.user && req.user._id && req.user_id === req.passport.user) {
      try {
        const user = await User.findById(req.session.user.id).exec()
        console.log('User - isLoggedIn : ', user.active)
        return user ? user.active : false
      } catch (e) {
        console.log('Err - isUserLoggedIn: ', e)
        return false
      }
    }
    return false
  },

  logout: async (req, res) => {
    try {
      if (req.isAuthenticated() && req.user) {
        console.log('\n** LOG-OUT USER **~\n')
        const user = await User.findById(req.user.id)
        user.active = false
        user.sessionkey = null
        await user.save()
        req.user && delete req.user
        req.logout()
        console.log('===> logout ::', req.session)
      }
      return res.render('index', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        page_title: 'Home',
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.error('ERR - logout : ', e)
      return res.render('500', { error: e.message })
    }
  }
}
