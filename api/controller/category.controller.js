const Category = require('../models/category.js')
const utils = require('../module/utils')

module.exports = {

  allCategories: async (req, res) => {
    try {
      const categories = await Category.find({})
      res.render('category/index', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        categories,
        page_title: 'Categories',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('ERR - allCategories: ', e)
      res.status(500).render('category/index', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        page_title: 'Categories',
        categories: [],
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage,
        errorMessage: 'Error getting Category'
      })
    }
  },

  getNew: async (req, res) => {
    return res.render('category/new', {
      is_authenticated: req.isAuthenticated(),
      user: req.user,
      category: new Category(),
      page_title: 'Add Category',
      haveNewMessage: req.haveNewMessage
    })
  },

  createCategory: async (req, res) => {
    const slugName = utils.slugify(req.body.name)
    const category = new Category({
      name: req.body.name,
      slugName,
      description: req.body.description
    })

    try {
      await category.save()
      res.status(200).redirect('/app/category', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        page_title: 'Add Category',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('ERR - createCategory: ', e)
      res.status(500).render('category/new', {
        category,
        errorMessage: 'Error creating Category'
      })
    }
  },

  deleteCategory: async (req, res, next) => {
    try {
      if (!req.params.id || typeof req.params.id !== 'string') {
        return
      }
      await Category.findByIdAndDelete(req.params.id)
      res.status(200).redirect('/category', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        page_title: 'Categories',
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage
      })
    } catch (e) {
      console.log('ERR - createCategory: ', e)
      res.status(500).render('category/index', {
        is_authenticated: req.isAuthenticated(),
        user: req.user,
        page_title: 'Categories',
        categories: await Category.find({}),
        back_url: req.back_url,
        haveNewMessage: req.haveNewMessage,
        errorMessage: 'Error deleting category'
      })
    }
  }
}
