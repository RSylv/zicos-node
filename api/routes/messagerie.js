const express = require('express')
const router = express.Router()
const messagerieController = require('../controller/messagerie.controller')

router.get('/', messagerieController.messagerie)
router.get('/new', messagerieController.getNew)
router.post('/new', messagerieController.postNew)
router.get('/:id', messagerieController.getOne)
router.post('/conversation', messagerieController.postConversationMessage)
router.delete('/conversation', messagerieController.deleteConversation)

module.exports = router
