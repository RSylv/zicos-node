const express = require('express')
const router = express.Router()
const mapConctroller = require('../controller/map.controller')

router.get('/', mapConctroller.getMap)

module.exports = router
