const express = require('express')
const router = express.Router()
const acccountController = require('../controller/account.controller')
const authController = require('../controller/auth.controller')

router.get('/', acccountController.myAccount)
router.post('/', authController.updateUser)
router.get('/user-details/:username', acccountController.userDetails)
router.get('/user-list', acccountController.list)

module.exports = router
