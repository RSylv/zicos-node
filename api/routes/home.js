const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  res.render('index', {
    is_authenticated: req.isAuthenticated(),
    user: req.user,
    page_title: 'Home',
    haveNewMessage: req.haveNewMessage
  })
})

router.get('/about-us', (req, res) => {
  res.render('about_us', {
    is_authenticated: req.isAuthenticated(),
    user: req.user,
    page_title: 'About-Us',
    haveNewMessage: req.haveNewMessage
  })
})

module.exports = router
