const express = require('express')
const router = express.Router()
const newsController = require('../controller/news.controller')

router.get('/', newsController.allNews)
router.get('/add', newsController.getAddNews)
router.get('/:slug', newsController.oneNews)
router.post('/', newsController.createNews)
router.post('/add-comment', newsController.addComment)
router.delete('/delete/:id', newsController.deleteNews)

module.exports = router
