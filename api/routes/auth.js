const express = require('express')
const router = express.Router()

const authController = require('../controller/auth.controller')
const userController = require('../controller/user.controller')
const checkRoute = require('../middleware/checkRoute')
const passport = require('passport')

// Login
router.get('/login', checkRoute.checkNotAuthenticated, authController.getLogin)
router.post('/login', checkRoute.checkNotAuthenticated, passport.authenticate('local', {
  failureRedirect: '/app/login',
  failureFlash: true,
  usernameField: 'username',
  passwordField: 'password'
}),
authController.logUser
)

// Registration
router.get('/register', checkRoute.checkNotAuthenticated, authController.getRegister)
router.post('/register', checkRoute.checkNotAuthenticated, authController.register)

// Logout
router.get('/logout', authController.logout)

// Update User Location
router.post('/test', userController.updateUserLocationValue)

module.exports = router
