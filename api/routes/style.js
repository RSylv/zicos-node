const express = require('express')
const router = express.Router()
const styleController = require('../controller/style.controller')

router.get('/', styleController.allStyles)
router.get('/new', styleController.getNew)
router.post('/', styleController.postNew)

module.exports = router
