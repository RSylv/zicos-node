const express = require('express')
const router = express.Router()
const userController = require('../controller/user.controller')
const adminController = require('../controller/admin.controller')
const generatorController = require('../controller/generator.controller')

function isAdmin (req, res, next) {
  if (!req.user || !req.user.isAdmin) {
    return res.redirect('/')
  }
  return next()
}

router.get('/', isAdmin, userController.getPanel)
router.get('/user/all', isAdmin, userController.getUserList)
router.get('/user/:id', isAdmin, userController.getUser)
router.get('/categories', isAdmin, adminController.getAllNewsCategories)
router.post('/category', isAdmin, adminController.createCategory)
router.delete('/delete-user', isAdmin, userController.deleteUser)
router.get('/rebuild-categories', isAdmin, adminController.saveInstrumentCategoriesOnDB)
router.get('/rebuild-instruments', isAdmin, adminController.saveInstrumentOnDB)

// TODO: Reajust generate function
router.get('/generate-user/:num', isAdmin, (req, res) => {
  const number = req.params.num ? parseInt(req.params.num) : 10
  console.log('\nnumber: ', number, '\n')
  generatorController.generateUser(number)
  return res.redirect('/administration')
})

module.exports = router
