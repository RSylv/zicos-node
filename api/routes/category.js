const express = require('express')
const router = express.Router()

const categoryController = require('../controller/category.controller')

// Toutes les Categories
router.get('/', categoryController.allCategories)

// Ajouter Une Categorie
router.get('/new', categoryController.getNew)
router.post('/', categoryController.createCategory)

module.exports = router
