module.exports = {
  initializeRoutes: (app) => {
    app.use('/', require('./home'))
    app.use('/app', require('./auth'))
    app.use('/app/map', require('./map'))
    app.use('/app/news', require('./news'))
    app.use('/app/style', require('./style'))
    app.use('/app/category', require('./category'))
    app.use('/app/account', require('./account'))
    app.use('/app/messagerie', require('./messagerie'))
    app.use('/administration', require('./admin'))
  }
}
