// const fs = require('fs');
// // Attach module to the console
// require('console-png').attachTo(console);
// // Read the image, let console-png output it to console
// console.png(fs.readFileSync(__dirname + '/logo2.png'));

const _ = require('lodash')

// https://www.madeintext.com/circle-symbols/
class Loaderlog {
  COLORS = {
    reset: '\x1b[0m',
    bright: '\x1b[1m',
    dim: '\x1b[2m',
    underscore: '\x1b[4m',
    blink: '\x1b[5m',
    reverse: '\x1b[7m',
    hidden: '\x1b[8m',

    fgBlack: '\x1b[30m',
    fgRed: '\x1b[31m',
    fgGreen: '\x1b[32m',
    fgYellow: '\x1b[33m',
    fgBlue: '\x1b[34m',
    fgMagenta: '\x1b[35m',
    fgCyan: '\x1b[36m',
    fgWhite: '\x1b[37m',

    bgBlack: '\x1b[40m',
    bgRed: '\x1b[41m',
    bgGreen: '\x1b[42m',
    bgYellow: '\x1b[43m',
    bgBlue: '\x1b[44m',
    bgMagenta: '\x1b[45m',
    bgCyan: '\x1b[46m',
    bgWhite: '\x1b[47m'
  }

  ANIMS = [
    '⓿❶❷❸❹❺❻❼❽❾❿',
    '➪➯➫➬➱',
    '\\|/-',
    '◐◒◑◓',
    '◜◝◞◟◜◝◞◟',
    '◜◝◞◟◜◝◡◞◟◡◯◦',
    '123456',
    ['CHARGING'],
    ['WAIT'],
    ['SERVICE'],
    ['READY']
  ]

  constructor () {
    this.reverse = false
    this.index = 10
    this.maxWordLength = 23
    this.animationArray = this.reverse ? _.reverse(this.getAnimation()) : this.getAnimation()
    this.timeout = null
    this.color = 'bgMagenta'
    this.colorValue = this.getColorValue()
  }

  wordToAnim (text) {
    text = text.toString().toUpperCase()

    if (!text) { text = '*' }
    if (text.length > this.maxWordLength) { throw new Error(`Text Can't have more than ${this.maxWordLength} parameters`) }

    const animationArray = []
    let str = ''
    for (let i = 0; i < text.length; i++) { str += ' ' }
    animationArray.push(str)
    text.split('').forEach((t, i) => {
      str = `${text.slice(0, ++i)}`
      if (str.length !== text.length) {
        for (let y = i; y < text.length; y++) { str += ' ' }
      }
      animationArray.push(str)
    })

    return animationArray
  }

  getAnimation () {
    this.index = Math.abs(this.index)
    return this.index <= this.ANIMS.length - 1
      ? this.formatArray(this.ANIMS[this.index])
      : this.formatArray(this.ANIMS[0])
  }

  getColorValue () {
    return Object.keys(this.COLORS).includes(this.color)
      ? this.COLORS[this.color]
      : this.COLORS.bgMagenta
  }

  formatArray (array) {
    return typeof array === 'string'
      ? array.split('')
      : typeof array === 'object' && array.length <= 1
        ? this.wordToAnim(array)
        : array
  }

  log (text = '', delay = 500) {
    const hasPoints = false
    text = text || '*'
    let x = 0
    let y = 0
    const points = ['     ', '.    ', '..   ', '...  ', '.... ', '.....', '.....']
    this.timeout = setInterval(() => {
      const string = `\r${this.colorValue} ${this.animationArray[y++]} ${text}${hasPoints ? points[x++] : ''} ${this.COLORS.reset}`
      process.stdout.write(string)
      y >= this.animationArray.length && (y = 0)
      x >= points.length && (x = 0)
    }, delay)
  }

  stop () {
    this.timeout !== null && clearInterval(this.timeout)
  }
}

module.exports = Loaderlog
