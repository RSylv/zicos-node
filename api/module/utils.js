const ObjectId = require('mongodb').ObjectId
const slugify = require('slugify')
const _ = require('lodash')
const fs = require('fs')

module.exports = {

  slugify: function (text) {
    return slugify(text, {
      remove: /[*+~.()'"!:@]/g,
      lower: true,
      strict: true
    })
  },

  slugifyV1: function (text, separator) {
    text = text.toString().toLowerCase().trim()

    const sets = [
      { to: 'a', from: '[ÀÁÂÃÅÆĀĂĄẠẢẤẦẨẪẬẮẰẲẴẶ]' },
      { to: 'ae', from: '[Ä]' },
      { to: 'c', from: '[ÇĆĈČ]' },
      { to: 'd', from: '[ÐĎĐÞ]' },
      { to: 'e', from: '[ÈÉÊËĒĔĖĘĚẸẺẼẾỀỂỄỆ]' },
      { to: 'g', from: '[ĜĞĢǴ]' },
      { to: 'h', from: '[ĤḦ]' },
      { to: 'i', from: '[ÌÍÎÏĨĪĮİỈỊ]' },
      { to: 'j', from: '[Ĵ]' },
      { to: 'ij', from: '[Ĳ]' },
      { to: 'k', from: '[Ķ]' },
      { to: 'l', from: '[ĹĻĽŁ]' },
      { to: 'm', from: '[Ḿ]' },
      { to: 'n', from: '[ÑŃŅŇ]' },
      { to: 'o', from: '[ÒÓÔÕØŌŎŐỌỎỐỒỔỖỘỚỜỞỠỢǪǬƠ]' },
      { to: 'oe', from: '[ŒÖ]' },
      { to: 'p', from: '[ṕ]' },
      { to: 'r', from: '[ŔŖŘ]' },
      { to: 's', from: '[ŚŜŞŠ]' },
      { to: 'ss', from: '[ß]' },
      { to: 't', from: '[ŢŤ]' },
      { to: 'u', from: '[ÙÚÛŨŪŬŮŰŲỤỦỨỪỬỮỰƯ]' },
      { to: 'ue', from: '[Ü]' },
      { to: 'w', from: '[ẂŴẀẄ]' },
      { to: 'x', from: '[ẍ]' },
      { to: 'y', from: '[ÝŶŸỲỴỶỸ]' },
      { to: 'z', from: '[ŹŻŽ]' },
      { to: '-', from: "[·/_,:;']" }
    ]

    sets.forEach((set) => {
      text = text.replace(new RegExp(set.from, 'gi'), set.to)
    })

    text = text
      .toString()
      .trim()
      .toLowerCase()
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(/&/g, '-and-') // Replace & with 'and'
      .replace(/[^\w\-]+/g, '') // Remove all non-word chars
      .replace(/\--+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, '') // Trim - from end of text

    if (typeof separator !== 'undefined' && separator !== '-') {
      text = text.replace(/-/g, separator)
    }

    return text
  },

  capitalize: function (text = '') {
    if (typeof text !== 'string') { return false }
    text = text.trim()
    return text.charAt(0).toUpperCase() + text.slice(1, text.length).toLowerCase()
  },

  /**
     * @param {String} id
     * @returns {Boolean}
     */
  isValidMongoId: function (id) {
    if (ObjectId.isValid(id)) {
      if ((String)(new ObjectId(id)) === id) { return true }
      return false
    }
    return false
  },

  /**
     * @param {Object[]} array
     * @returns {Promise<Object[]>}
     */
  autoPopulateObject: async function (array) {
    return await Promise.all(array.map(async (object, i) => {
      if (_.isArray(object)) {
        module.exports.autoPopulateObject(object)
      } else if (typeof object === 'string' && module.exports.isValidMongoId(object)) {
        console.log('OBJ-K: ', Object.keys(object))
        if (Object.keys(array)[i].includes('user')) {
          console.log('User')
          array[i] = await ['User'].findById(object)
        } else if (Object.keys(array)[i].includes('message')) {
          console.log('Messagerie')
          array[i] = await ['Messagerie'].findById(object)
        }
      }
    }))
  },

  shuffle: function (array) {
    if (!_.isArray(array)) {
      throw new Error('Input to shuffle be be an non empty, or more than one element, array')
    }

    let currentIndex = array.length; let randomIndex

    // While there remain elements to shuffle.
    while (currentIndex !== 0) {
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex)
      currentIndex--;

      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]]
    }

    return array
  },

  getAllFileNameInPath: (path, ext = 'png', prefix = '/image/animals/') => {
    return fs.readdirSync(__dirname.split('api')[0] + path)
      .map(f => f.slice(-4) === `.${ext}` && `${prefix}${f}`)
      .filter(f => f)
  },

  getMongoError: (err) => {
    err = err.message || err
    if (err.startsWith('MongoError: E11000 duplicate')) {
      console.log('ERR: ', err)
      return 'duplicate'
    }
    return false
  },

  getDateAndTime: (createdAt, updatedAt = null) => {
    const dateFormatted = updatedAt
      ? new Date(updatedAt).toLocaleString('fr-FR', { timeZone: 'UTC' })
      : new Date(createdAt).toLocaleString('fr-FR', { timeZone: 'UTC' })
    return {
      date: dateFormatted.split(',')[0].trim(),
      time: dateFormatted.split(',')[1].trim()
    }
  }
}
