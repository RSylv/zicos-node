/* eslint-disable camelcase */
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const morgan = require('morgan')
const session = require('express-session')
const expressLayouts = require('express-ejs-layouts')
const passport = require('./api/config/passport-config')
const flash = require('express-flash')
const methodOverride = require('method-override')
const cors = require('cors')
const config = require('./api/config/server')

// Connect mongoose
require('./api/config/mongoose')(config.database_url)

// Route
const appRouting = require('./api/routes/_index')

const app = express()

// view engine setup
const static_path = path.join(__dirname, '/public')
const template_path = path.join(__dirname, './template/views')
const layout_path = path.join(__dirname, './template/layouts/layout')
app.set('views', template_path)
app.set('layout', layout_path)
app.set('view engine', 'ejs')

// memory unleaked
app.set('trust proxy', 1)

// uncomment after placing favicon in /public
app.use(favicon(path.join(__dirname, '/public/favicon.ico')))

app.use(morgan('tiny'))
app.use(express.json())
app.use(expressLayouts)
app.use(express.static(static_path))
app.use(express.urlencoded({ limit: '10mb', extended: true }))
app.use(cors())
app.use(flash())
app.use(session({
  secret: config.session_secret,
  // cookie: {
  //     secure: true,
  //     maxAge : 1000 * 60 * 3,
  // },
  resave: false,
  saveUninitialized: false
  // store: MongoStore.create({ mongoUrl: process.env.DATABASE_URL }),
}))

// Configure passport middleware
app.use(passport.initialize())
app.use(passport.session())
app.use(methodOverride('_method'))

// security middleware
app.use(require('./api/middleware/auth').redirections)

// ROUTES
appRouting.initializeRoutes(app)
// console.log('\nROUTES\nname === router:\n', app._router.stack.map(l => l.name === 'router' && l.regexp).filter(n => n))

// |----------------------------|
// |----<  ERROR HANDLERS  >----|
// |----------------------------|

// 404 Redirections
app.all('*', function (req, res) {
  res.status(404)
  return res.render('404', {
    page_title: 'Page not Found...',
    is_authenticated: req.isAuthenticated(),
    user: req.user,
    back_url: req.back_url
  })
})

// 500 Redirections
// development error handler
// will print stacktrace
if (app.get('env') !== 'production') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    return res.render('error', {
      is_authenticated: req.isAuthenticated(),
      user: req.user,
      message: err.message,
      error: err
    })
  })
} else {
  // production error handler
  // no stacktraces leaked to user
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: {}
    })
  })
}

module.exports = app
