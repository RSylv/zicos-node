const CACHE_NAME = 'sw-cache-example'
const toCache = [
  '/'
]

// used to register the service worker
self.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function (cache) {
        return cache.addAll(toCache)
      })
      .then(self.skipWaiting())
  )
})

// used to intercept requests so we can check for the file or data in the cache
self.addEventListener('fetch', function (event) {
  event.respondWith(
    fetch(event.request)
      .catch(() => {
        return caches.open(CACHE_NAME)
          .then((cache) => {
            return cache.match(event.request)
          })
      })
  )
})

// this event triggers when the service worker activates
self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys()
      .then((keyList) => {
        // eslint-disable-next-line array-callback-return
        return Promise.all(keyList.map((key) => {
          if (key !== CACHE_NAME) {
            console.log('[ServiceWorker] Removing old cache', key)
            return caches.delete(key)
          }
        }))
      })
      .then(() => self.clients.claim())
  )
})
