/**
 * GEOLOCALISATION SCRIPT
 *
 * - Enable only on connexion
 */
const locationErr = document.getElementById('geolocation_err')
const options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
}

function success (pos) {
  const crd = pos.coords
  const coords = {
    accuracy: crd.accuracy,
    altitude: crd.altitude,
    latitude: crd.latitude,
    longitude: crd.longitude,
    speed: crd.speed
  }
  if (getBaseUrl()) {
    sendLocationData(coords)
  }
};

function locationError (error) {
  let message
  switch (error.code) {
    case 1:
      message = 'Please activate your geolocation to be placed in the right position on the map'
      break
    case error.PERMISSION_DENIED:
      message = 'You denied the request for geolocation.'
      break
    case error.POSITION_UNAVAILABLE:
      message = 'Location information is currently unavailable.'
      break
    case error.TIMEOUT:
      message = 'Request for your location timed out.'
      break
    case error.UNKNOWN_ERROR:
      message = 'An unknown error occurred.'
      break
  };

  console.warn(message)

  // Display error message (pop-up)
  if (message) {
    locationErr.classList.remove('hidden')
    locationErr.textContent = message
    setTimeout(() => {
      locationErr.classList.add('move-left')

      setTimeout(() => {
        locationErr.textContent = ''
        locationErr.classList.add('hidden')
      }, 1000)
    }, 2000)
  }
};

function getBaseUrl () {
  const getUrl = window.location
  const baseUrl = getUrl.protocol + '//' + getUrl.host + '/'
  return baseUrl
};

function sendLocationData (data) {
  const url = getBaseUrl() + 'app/test'
  const body = JSON.stringify(data)

  const req = new XMLHttpRequest()
  req.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      console.log('-- RES --', this.responseText)
    }
  }
  req.open('POST', url, true)
  req.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
  req.send(body)
};

navigator.geolocation.getCurrentPosition(success, locationError, options)
