import ImageList from './layout/ImageList.js'
import MenuButton from './button/MenuButton.js'
import CustomButton from './button/CustomButton.js'
import UserListCard from './card/UserListCard.js'
import UserPublicCard from './card/UserPublicCard.js'
import InputCustom from './form/InputCustom.js'

/**
 * Definitionn des customs elements
 */
customElements.define('image-list', ImageList)
customElements.define('menu-button', MenuButton)
customElements.define('custom-button', CustomButton)
customElements.define('user-list-card', UserListCard)
customElements.define('input-custom', InputCustom)
customElements.define('user-public-card', UserPublicCard)
