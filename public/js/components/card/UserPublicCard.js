export default class UserPublicCard extends HTMLElement {
  constructor () {
    super()

    this.shadow = this.attachShadow({ mode: 'open' })

    // Props
    this.avatar_url = this.hasAttribute('avatar-url') ? this.getAttribute('avatar-url') : undefined
    this.name = this.hasAttribute('name') ? this.getAttribute('name') : undefined
    this.email = this.hasAttribute('email') ? this.getAttribute('email') : undefined
    this.place = this.hasAttribute('place') ? this.getAttribute('place') : undefined
    this.level = this.hasAttribute('level') ? this.getAttribute('level') : undefined
    this.musicalStyle = this.hasAttribute('musical-style') ? this.getAttribute('musical-style') : undefined
    this.instruments = this.hasAttribute('instruments') ? this.getAttribute('instruments') : undefined
    this.want_to_learn = this.hasAttribute('want-to-learn') ? this.getAttribute('want-to-learn') : undefined
    this.latitude = this.hasAttribute('lat') ? this.getAttribute('lat') : undefined
    this.longitude = this.hasAttribute('lon') ? this.getAttribute('lon') : undefined

    // Html
    const userPublicCard = document.createElement('div')
    userPublicCard.setAttribute('class', 'user-public-card')
    userPublicCard.innerHTML = `
        <div class="avatar"><img src="${this.avatar_url}" alt=""></div>
        <hr>
        <p>pseudo: <span>${this.name}</span></p>
        <p>email: <span>${this.email}</span></p>
        <p>Ville: <span>${this.place}</span></p>
        <p>level: <span>${this.level}</span></p>
        <p>style: </p>
        <p><span>${this.musicalStyle}</span></p>
        <p>instruments: </p>
        <p><span>${this.instruments}</span></p>
        <p>TékiToi ?</p>
        <p class="presentation">${this.want_to_learn}</p>
        <hr>
        <div class="button-contact">
            ${this.latitude && this.longitude
                ? `<custom-button 
                    type="secondary"
                    rounded
                    size="M"
                    link="/app/map?lat=${this.latitude}&lon=${this.longitude}"
                    img="/icon/location.svg"
                    title="Retrouver ce membre sur la carte">
                </custom-button>`
                : ''
            }
            <custom-button
                type="secondary"
                rounded
                size="M"
                link="/app/messagerie/new?username=${this.name}"
                img="/icon/send-message.svg"
                title="Envoyer un message">
            </custom-button>
        </div>
        `

    // CSS
    const style = document.createElement('style')
    style.textContent = `
        .user-public-card {
            display: flex;
            flex-direction: column;
            border-radius: .5rem;
            background-color: var(--primary);
            padding: 1rem;
            margin: 1rem auto;
            max-width: 20rem;
        }
        hr {
            width: 100%;
            opacity: .1;
            margin: .5rem 0 .25rem 0;
        }
        .avatar {
            margin:auto;
            height: 8rem;
            overflow: hidden;
            padding-bottom: 1rem;
            display: grid;
            place-items: center;
        }
        .avatar img {
            width: auto;
            height: 8rem;
        }
        .button-contact {
            display: flex;
            justify-content: space-between;
            width: 100%;
        }
        p {
            display: flex;
            align-items: center;
            justify-content: space-between;
            color: var(--text-muted);
            margin: .5rem;
        }
        p span {
            color: var(--text);
            margin-left: .5rem;
        }
        .presentation {
            margin-left: .5rem;
            color: var(--text);
            white-space: pre-wrap;
        }
        `

    // AppendChild
    this.shadow.appendChild(style)
    this.shadow.appendChild(userPublicCard)
  }
}
