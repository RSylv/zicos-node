export default class UserListCard extends HTMLElement {
  constructor () {
    super()

    this.shadow = this.attachShadow({ mode: 'open' })

    // Get Props (username, isactive, style_text, latitude, longitude, instruments_text)
    this.isactive = this.hasAttribute('isactive')
    this.name = this.hasAttribute('name') ? this.getAttribute('name') : 'John'
    this.musicalStyle = this.hasAttribute('musical-style') ? this.getAttribute('musical-style') : 'Metal'
    this.latitude = this.hasAttribute('lat') ? this.getAttribute('lat') : null
    this.longitude = this.hasAttribute('lon') ? this.getAttribute('lon') : null
    this.instruments = this.hasAttribute('instruments') ? this.getAttribute('instruments') : 'Bigniou'

    const card = document.createElement('div')
    card.setAttribute('class', 'user-card')
    card.innerHTML = `
        <div class="user-card-header">
            <div>
                <h3 class="name">
                    <span>${this.isactive ? '🟢' : '⚫️'}</span>
                    ${this.capitalize(this.name)}
                </h3>
                <div class="style">
                    <img src="/icon/headset.svg"> ${this.musicalStyle}
                </div>
            </div>
            <div class="user-card-button">
                ${this.latitude && this.longitude
                    ? `<custom-button 
                        type="secondary" 
                        rounded 
                        outlined
                        size="M" 
                        link="../map?lat=${this.latitude}&lon=${this.longitude}"
                        img="/icon/location.svg"
                        title="Retrouver ce membre sur la carte">
                    </custom-button>`
                    : ''
                }
                <custom-button 
                    type="secondary" 
                    rounded 
                    size="M" 
                    link="../messagerie/new?username=${this.name}"
                    img="/icon/send-message.svg"
                    title="Envoyer un message">
                </custom-button>
            </div>
        </div>
        <hr>
        <div class="instruments">
            <img src="/icon/note.svg"> ${this.instruments}
        </div>
        `

    const style = document.createElement('style')
    style.textContent = `
        hr {
            width: 100%;
            opacity: .1;
            margin: .5rem 0 .25rem 0;
        }
        .user-card {
            display: flex;
            flex-direction: column;
            border-radius: .25rem;
            background-color: var(--primary);
            padding: .5rem .75rem;
            margin: .25rem 0;
        }
        .user-card-header {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }
        .user-card-button {
            display: flex;
        }
        .user-card-header .name {
            font-size: 1rem;
            font-weight: 600;
            margin: .15rem 0;
            display: flex;
            align-item: center;
        }
        .user-card-header .name span{
            font-size: .35rem;
            margin-right: .52rem;
            margin-left: .27rem;
        }
        .user-card-header .style {
            font-size: ,9rem;
            font-weight: 200;
            margin-top: .35rem
        }
        .user-card-header .style img {
            height: .9rem;
            margin-right: .1rem;
        }
        .user-card .instruments {
            margin: .5rem 0;
            display: flex;
            justify-content: flex-start;
            font-size: .85rem;
            font-weight: 200;
        }
        .user-card .instruments img {
            height: .65rem;
            margin-right: .5rem;
            margin-left: .1rem;
        }
        `

    this.shadow.appendChild(style)
    this.shadow.appendChild(card)
  }

  capitalize (str) {
    return str.slice(0, 1).toUpperCase() + str.slice(1, str.length)
  }
}
