export default class InputCustom extends HTMLElement {
  constructor () {
    super()

    this.shadow = this.attachShadow({ mode: 'closed' })

    const uniqueId = Date.now()

    this.inputContainer = document.createElement('div')
    this.inputContainer.setAttribute('class', 'input-custom')

    this.input = document.createElement('input')
    this.input.setAttribute('id', uniqueId)
    this.input.setAttribute('required', true)

    if (this.hasAttribute('type')) {
      this.input.setAttribute(
        'type',
        this.getAttribute('type')
      )
    }

    if (this.hasAttribute('placeholder')) {
      this.input.setAttribute(
        'placeholder',
        this.getAttribute('placeholder')
      )
    }

    if (this.hasAttribute('label')) {
      const label = document.createElement('label')
      label.setAttribute('for', uniqueId)
      label.textContent = this.getAttribute('label')
      this.inputContainer.appendChild(label)
    }

    const style = document.createElement('style')
    style.textContent = `
            .input-custom {
                display: flex;
                flex-direction: column;
                padding: .5rem .25rem;
            }
            label {
                font-size: .75rem;
                font-weight: 150;
                padding: .15rem 0;
                text-transform: uppercase;
            }
            input {
                font-size: .95rem;
                padding: .5rem;
                border-radius: .25rem;
            }
        `

    this.inputContainer.appendChild(this.input)
    this.shadow.appendChild(style)
    this.shadow.appendChild(this.inputContainer)
  }
}
