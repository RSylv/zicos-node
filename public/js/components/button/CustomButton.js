export default class CustomButton extends HTMLElement {
  constructor () {
    super()

    this.shadow = this.attachShadow({ mode: 'open' })

    this.btnlink = this.hasAttribute('link') ? this.getAttribute('link') : false

    // https://isotropic.co/tool/hex-color-to-css-filter/
    const svgColors = {
      primary: 'invert(0%) sepia(98%) saturate(0%) hue-rotate(331deg) brightness(104%) contrast(102%)',
      secondary: 'invert(26%) sepia(6%) saturate(102%) hue-rotate(71deg) brightness(95%) contrast(88%)',
      ternary: 'invert(41%) sepia(43%) saturate(846%) hue-rotate(147deg) brightness(99%) contrast(93%)',
      text: 'invert(100%) sepia(100%) saturate(0%) hue-rotate(67deg) brightness(103%) contrast(101%)',
      success: 'invert(33%) sepia(69%) saturate(710%) hue-rotate(75deg) brightness(105%) contrast(92%)',
      warning: 'invert(49%) sepia(47%) saturate(755%) hue-rotate(12deg) brightness(93%) contrast(87%)',
      error: 'invert(25%) sepia(21%) saturate(5403%) hue-rotate(341deg) brightness(106%) contrast(108%)',
      submit: 'invert(21%) sepia(88%) saturate(1720%) hue-rotate(212deg) brightness(100%) contrast(107%);'
    }

    // Html Elements
    const slot = document.createElement('slot')
    if (this.btnlink) {
      this.button = document.createElement('a')
      this.button.setAttribute('href', this.btnlink)
    } else {
      this.button = document.createElement('button')
    }
    this.button.setAttribute('class', 'custom-button')

    // Props
    this.type = this.getAttribute('type') || 'primary'
    this.type === 'submit' && this.button.setAttribute('type', 'submit')
    this.rounded = !!this.hasAttribute('rounded')
    this.img = document.createElement('img')
    this.img.setAttribute('class', 'emb')
    const imgLink = this.hasAttribute('img') ? this.getAttribute('img') : false
    let isSvg = false
    if (imgLink) {
      isSvg = imgLink.slice(-3) === 'svg'
    }
    if (this.hasAttribute('title')) { this.button.title = this.getAttribute('title') }

    // Size
    const sizeLetter = (this.getAttribute('size') && this.getAttribute('size').toUpperCase()) || 'S'
    const baseSize = { key: 'S', font: '.85rem' }
    const sizeArray = [
      { key: 'XS', font: '.65rem' },
      { key: 'S', font: '.9rem' },
      { key: 'M', font: '1.15rem' },
      { key: 'L', font: '1.5rem' },
      { key: 'XL', font: '1.85rem' },
      { key: 'XXL', font: '2.5rem' }
    ]
    this.size = sizeArray.find(s => s.key === sizeLetter) || baseSize

    this.outlined = !!this.hasAttribute('outlined')

    // CSS
    const style = document.createElement('style')
    style.textContent = `
        .custom-button {
            text-decoration: none;
            margin: .25rem;
            user-select: none;
            cursor: pointer;
            ${this.outlined ? `border: 1px solid var(--${this.type});background-color: transparent;color: var(--${this.type});` : `border: 1px solid transparent;background-color: var(--${this.type});color: var(--text);`}
            padding: .25rem .75rem;
            ${this.rounded ? 'border-radius: .15rem;' : ''}
            font-size: ${this.size.font};
            transition: .2s;
            display: grid;
            place-items: center;
            width: fit-content;
        }
        .custom-button .emb {
            height: ${Number(this.size.font.slice(0, -3)) + 0.5}rem;
            ${this.outlined && isSvg ? `filter: ${svgColors[this.type]};` : ''}
        }
        .custom-button:hover {
            ${this.outlined ? `background-color: var(--${this.type});color: var(--text);` : 'filter: brightness(1.115);'}
        }
        ${this.outlined && isSvg
? `
        .custom-button:hover .emb {
            filter: none;
        }
        `
 : ''}
        .custom-button:active {
            transform: scale(1.025);
        }
        `

    // Append
    if (imgLink) {
      this.img.src = imgLink
      this.button.appendChild(this.img)
    } else {
      this.button.appendChild(slot)
    }
    this.shadow.appendChild(style)
    this.shadow.appendChild(this.button)
  }
}
