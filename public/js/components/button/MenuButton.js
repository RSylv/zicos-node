export default class MenuButton extends HTMLElement {
  constructor () {
    super()

    // Création d'une racine fantôme
    this.shadow = this.attachShadow({ mode: 'open' })

    // Création des elements html
    const alink = document.createElement('a')
    if (this.hasAttribute('link')) {
      alink.setAttribute('href', this.getAttribute('link'))
    } else {
      alink.setAttribute('href', '/')
    }

    const div = document.createElement('div')

    const img = document.createElement('img')
    if (this.hasAttribute('img')) {
      img.setAttribute('src', this.getAttribute('img'))
    } else {
      img.setAttribute('src', 'icon/home.svg')
    }

    const alert = document.createElement('span')

    // Création du CSS à appliquer au dom fantôme
    const style = document.createElement('style')
    const baseCss = `
        a {
            width: 100%;
            height: 100%;
            display: grid;
            align-items: center;
            justify-content: center;
            text-decoration: none;
        }

        a div {
            border-radius: 50%;
            width: 2.75rem;
            height: 2.75rem;
            display: grid;
            align-items: center;
            justify-content: center;
        }

        a div img {
            width: 1.5rem;
            height: 1.5rem;
        }

        a:hover div {
            background-color: var(--muted-hover);
        }
        `

    // Ajouter un point rouge si il y a des news
    if (this.hasAttribute('hasnews')) {
      style.textContent = baseCss + `
            a div span {
                position: relative;
                width: 7px;
                height: 7px;
                border-radius: 50%;
                background-color: red;
                top: 5px;
                right: -20px;
            }
            a div {
                align-items: baseline;
            }
            `
      div.appendChild(alert)
    } else {
      style.textContent = baseCss
    }

    // Attacher les éléments créés au dom fantôme
    this.shadow.appendChild(style)
    div.appendChild(img)
    alink.appendChild(div)
    this.shadow.appendChild(alink)
  }
}
