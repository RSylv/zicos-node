export default class ImageList extends HTMLElement {
  constructor () {
    super()

    // this.shadow = this.attachShadow({ mode: 'open' })

    this.imgSize = '6rem'
    this.imageBoxs = []

    this.imagesContainer = document.createElement('div')
    this.imagesContainer.setAttribute('class', 'images-contaner')// avatar-selection

    this.selected_avatar = this.hasAttribute('selected') ? this.getAttribute('selected').trim() : false

    this.inputHidden = document.createElement('input')
    this.inputHidden.setAttribute('type', 'hidden')
    this.inputHidden.setAttribute('name', this.hasAttribute('input-name') ? this.getAttribute('input-name') : '')

    this.tempArray = []
    if (this.hasAttribute('imageList')) {
      this.images = this.getAttribute('imageList').split(',')
    }

    this.tempSelectedImagebox = null

    this.images.forEach(imP => {
      imP = imP.trim()
      const imageBox = document.createElement('imageBox')// register-avatar
      imageBox.setAttribute('class', 'image-box')

      const img = document.createElement('img')
      img.setAttribute('src', `${imP}`)
      img.setAttribute('alt', `${imP.slice(imP.lastIndexOf('/') + 1, imP.length - 4)}`)
      img.setAttribute('title', `${imP.slice(imP.lastIndexOf('/') + 1, imP.length - 4)}`)

      imageBox.addEventListener('click', () => {
        if (!imageBox.classList.contains('active')) {
          [...this.imagesContainer.childNodes].forEach(e => {
            if (e !== imageBox) {
              e.remove()
            }
          })
          imageBox.classList.add('active')
          this.imagesContainer.classList.add('one-image')
          this.inputHidden.setAttribute('value', imP)
        } else {
          this.tempArray.forEach(e => {
            this.imagesContainer.append(e)
          })
          imageBox.classList.remove('active')
          this.imagesContainer.classList.remove('one-image')
          this.inputHidden.setAttribute('value', '')
        }
      })

      this.imageBoxs.push(imageBox)
      imageBox.appendChild(img)
      this.imagesContainer.appendChild(imageBox)
      this.tempArray.push(imageBox)

      // SELECTED
      if (this.selected_avatar && imP === this.selected_avatar) {
        console.log('found')
        this.inputHidden.setAttribute('value', imP)
        this.tempSelectedImagebox = imageBox
        console.log('FOUND:: ', this.tempSelectedImagebox)
      }
    })

    // CSS
    const style = document.createElement('style')
    style.textContent = `
        .images-contaner {
            user-select: none;
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
            align-items: center;
            width: 90vw;
            height: 10rem;
            overflow-x: auto;
        }
        .images-contaner.one-image {
            justify-content: center;
        }
        .image-box {
            height: ${this.imgSize};
            width: ${this.imgSize};
            border-radius: 50%;
            margin: .1rem;
            transition: .2s linear;
        }
        .image-box img {
            height: 100%;
            width: ${this.imgSize};
        }
        .image-box:hover {
            cursor: pointer;
            transform: scale(1.2);
        }
        .image-box.active {
            background: var(--muted-hover) !important;
            transform: scale(1.2) !important;
        }
        .image-box.grayscale {
            transform: grayscale(1) !important;
            opacity: .01 !important;
        }
        `

    // APPEND
    this.appendChild(style)
    this.appendChild(this.imagesContainer)
    this.appendChild(this.inputHidden)

    // SELECT ONE
    if (this.selected_avatar) {
      const elements = [...this.imagesContainer.childNodes]
      elements.forEach(e => {
        if (e !== this.tempSelectedImagebox) {
          e.remove()
        }
      })
      this.tempSelectedImagebox.classList.add('active')
      this.imagesContainer.classList.add('one-image')
    }
  }

  disconnectedCallback () {
    this.imageBoxs.length && this.imageBoxs.forEach(imageBox => {
      imageBox.removeEventListener()
    })
  }
}
