let deferredPrompt

const addBtn = document.createElement('button')
addBtn.className = 'install-app-button btn hidden'
addBtn.textContent = 'Installer'

const isInStandaloneMode = () =>
  ((window.matchMedia('(display-mode: fullscreen)').matches) || (window.navigator.fullscreen)) ||
    ((window.matchMedia('(display-mode: standalone)').matches) || (window.navigator.standalone)) ||
    document.referrer.includes('android-app://')

if (isInStandaloneMode()) {
  console.log('webapp is installed 🌵')
} else {
  console.log('webapp is not installed 🐙')

  // Reset screen
  document.querySelector('main').innerHTML = ''

  // Create Elements
  const divContainer = document.createElement('div')
  divContainer.className = 'install-page'
  divContainer.innerHTML = `
    <img src="/icon/rocker.svg">
    <h1>Bienvenue sur Zicos</h1>
    <p>L'appplication qui met en lien les musichien.ene.s</p>`
  divContainer.appendChild(addBtn)

  // Append Elements in main
  document.querySelector('main').append(divContainer)

  window.addEventListener('beforeinstallprompt', (e) => {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault()

    // Stash the event so it can be triggered later.
    deferredPrompt = e

    // Update UI to notify the user they can add to home screen
    addBtn.classList.remove('hidden')

    addBtn.addEventListener('click', (e) => {
      // hide our user interface that shows our A2HS button
      addBtn.classList.add('hidden')

      // Show the prompt
      deferredPrompt.prompt()

      // Wait for the user to respond to the prompt
      deferredPrompt.userChoice.then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          console.log('User accepted the A2HS prompt 🤝')
        } else {
          console.log('User dismissed the A2HS prompt 👊')
        }
        deferredPrompt = null
      })
    })
  })
}
